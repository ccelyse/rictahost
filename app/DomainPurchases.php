<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomainPurchases extends Model
{
    protected $table = "domain_purchases";
    protected $fillable = ['id','user_id','domain_name','price','quantity',
        'domain_id','ns1','ns2','ns3','ns4','reg_period','exp_date','subtotal','tax',
        'price','total','order_status','error'];
}
