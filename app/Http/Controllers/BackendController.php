<?php

namespace App\Http\Controllers;

use App\Countries;
use App\DomainPurchases;
use App\momotransaction;
use App\RenewDomains;
use App\User;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Metaregistrar\EPP\eppConnection;
use Metaregistrar\EPP\eppContactHandle;
use Metaregistrar\EPP\eppDomain;
use Metaregistrar\EPP\eppHost;
use Metaregistrar\EPP\eppInfoDomainRequest;
use Metaregistrar\EPP\eppUpdateDomainRequest;

class BackendController extends Controller
{
    public function Dashboard(){
//        $userId = Auth::id();
        $completebilling = User::where('id',Auth::id())->value('billing_status');
        $domains = DomainPurchases::where('user_id',Auth::id())->where('order_status','SUCCESSFUL')->count();
        return view('Client.Dashboard')->with(['domains'=>$domains,'completebilling'=>$completebilling]);
    }
    public function AdminDashboard(){
//        $userId = Auth::id();
        $completebilling = User::where('id',Auth::id())->value('billing_status');
        $domains = DomainPurchases::where('user_id',Auth::id())->where('order_status','SUCCESSFUL')->count();
        $bought_domains = DomainPurchases::where('order_status','SUCCESSFUL')->count();
        $all_users = User::where('role','user')->count();
        return view('Admin.AdminDashboard')->with(['domains'=>$domains,'completebilling'=>$completebilling,
            'bought_domains'=>$bought_domains,'all_users'=>$all_users]);
    }
    public function DashboardStats(){
        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');

        $jancount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->count();
        $febcount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->count();
        $marcount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->count();
        $apcount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->count();
        $maycount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->count();
        $junecount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->count();
        $julycount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->count();
        $augcount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->count();
        $septcount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->count();
        $octcount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->count();
        $novcount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->count();
        $deccount = DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->count();

        return response()->json([
            'response_message' => "success",
            'response_status' =>400,
            'User_' =>Auth::id(),
            'jan' =>$jancount,
            'feb' =>$febcount,
            'march' =>$marcount,
            'april' =>$apcount,
            'may' =>$maycount,
            'july' =>$julycount,
            'june' =>$junecount,
            'august' =>$augcount,
            'september' =>$septcount,
            'october' =>$octcount,
            'november' =>$november,
            'december' =>$deccount,
        ]);
    }
    public function SystemUser(){
        $users = User::where('users.id',Auth::id())
            ->join('apps_countries', 'users.billing_country', '=', 'apps_countries.country_code')
            ->select('users.*', 'apps_countries.country_name')
            ->get();
        if(0 == count($users)){
            $getusers = User::where('users.id',Auth::id())->get();
        }else{
            $getusers = User::where('users.id',Auth::id())
                ->join('apps_countries', 'users.billing_country', '=', 'apps_countries.country_code')
                ->select('users.*', 'apps_countries.country_name')
                ->get();
        }
        $list = Countries::all();
        return view('Client.SystemUser')->with(['users'=>$getusers,'list'=>$list]);
    }
    public function UpdateUserDetails(Request $request){
        $all = $request->all();
//        dd($all);
        $update = User::where('id',Auth::id())->update(['name'=>$request['name'],'email'=>$request['email'],'billing_business_name'=>$request['billing_business_name'],'billing_tin_number'=>$request['billing_tin_number']
            ,'billing_phone'=>$request['billing_phone'],'billing_address'=>$request['billing_address'],
            'billing_postal'=>$request['billing_postal'],'billing_city'=>$request['billing_city'],
            'billing_country'=>$request['billing_country'],'billing_status'=>'completed']);

        return back()->with('success','You have successfully updated Account and billing information');
    }
    public function DomainsManage(){
        $domains = DomainPurchases::where('user_id',Auth::id())->where('order_status','SUCCESSFUL')->get();
        return view('Client.DomainsManage')->with(['domains'=>$domains]);
    }
    public function AdminDomainsManage(){
        $domains = DomainPurchases::where('user_id',Auth::id())->where('order_status','SUCCESSFUL')->get();
        return view('Admin.DomainsManage')->with(['domains'=>$domains]);
    }
    public function AllDomainsManage(){
        $domains = DomainPurchases::where('order_status','SUCCESSFUL')->get();
        return view('Admin.AllDomainsManage')->with(['domains'=>$domains]);
    }
    public function AllAccounts(){
        $accounts = User::all();
        return view('Admin.AllAccounts')->with(['accounts'=>$accounts]);
    }
    public function UserDomainName(Request $request){
        $id = $request['id'];
        $domains = DomainPurchases::where('user_id',$id)->where('order_status','SUCCESSFUL')->get();
        return view('Admin.UserDomainName')->with(['domains'=>$domains]);
    }
    public function DomainsManageResponse(){
        $domains = DomainPurchases::where('user_id',Auth::id())->where('order_status','SUCCESSFUL')->get();
        return view('Client.DomainsManageResponse')->with(['domains'=>$domains]);
    }
    public function UpdateNameServers(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $ns1 = $request['ns1'];
        $ns2 = $request['ns2'];
        $domainname = $request['domainname'];
        $nameservers = array($request['ns1'], $request['ns2']);
        try {
            // Please enter your own settings file here under before using this example
            if ($conn = eppConnection::create(base_path(). '/settings.ini')) {
                // Connect to the EPP server
                if ($conn->login()) {
                   modifydomain($conn, $domainname, null, null, null, null, array($request['ns1'], $request['ns2']),$id,$ns1,$ns2);
                    $conn->logout();
                }
            }
        } catch (eppException $e) {
            echo $e->getMessage() . "\n";
        }

//        return back()->with('success','You have successfully updated updated name servers');
    }
    public function TransferDomain(Request $request){
        $all = $request->all();
        $domainname = $request['domainname'];
        $authcode = $request['authcode'];
        try {
            // Please enter your own settings file here under before using this example
            if ($conn = eppConnection::create(base_path(). '/settings.ini')) {
                // Connect and login to the EPP server
                if ($conn->login()) {
                    transferdomain($conn, $domainname, $authcode);
                    $conn->logout();
                }
            }
        } catch (eppException $e) {
            echo "ERROR: " . $e->getMessage() . "\n\n";
        }
    }
    public function BillingHistory(){
        $billings = momotransaction::where('user_id',Auth::id())->where('payment_status','SUCCESSFUL')->get();
        return view('Client.BillingHistory')->with(['billings'=>$billings]);
    }
    public function AllBillingHistory(){
        $billings = momotransaction::all();
        return view('Admin.AllBillingHistory')->with(['billings'=>$billings]);
    }

    public function RenewDomain(Request $request){
        $all = $request->all();
        $domainname = $request['domainname'];
        $id = $request['id'];
        $period = $request['period'];
        $expdate = $request['expdate'];
        $updatedexp = date("Y-m-d", strtotime("+$period years", strtotime($expdate)));

        $phonenumber = $request['payment_number'];
        $amount = $request['domain_price'] * $period;
//        $amount = "50";
        $idt = mt_rand(10, 99);
        //Generating Payment Gateway
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=SDHost&payment_code=$idt",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: akokanya.com",
                "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                "User-Agent: PostmanRuntime/7.11.0",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache",
                "content-length: "
            ),
        ));

        $responseapi = curl_exec($curl);
//            dd($response);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            $title="Not Connected";
            $message="Sorry you do not have no internet connection";
            $response = "Sorry you do not have no internet connection";
            //return redirect()->route('balance')->with($title,$message);
            echo "$err";
            return back()->with('success',$response);

        } else {
            //echo $response;

            //Showing Status
            //second part Status checking

            $responseid=json_decode($responseapi);
//                        dd($responseid);

            $curl = curl_init();

            if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                //checking balance
                $maintitle="Balance";
                $title="Not Enough Balance";
                $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                //return redirect()->route('balance')->with($title,$message);
//                    echo "$response";
                return back()->with('success',$response);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                //User not registered in Momo
                $maintitle="Not Registered";
                $title="Not Registered";
                $response="Sorry you're not registered with MTN Mobile Rwanda";
//                    echo "$response";
                return back()->with('success',$response);
            }
            elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                //User not registered in Momo
                $maintitle="Minimum amount";
                $title="Minimum amount";
                $response="Sorry the minimum amount to send is 100Frw";
//                    echo "$response";
                return back()->with('success',$response);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                //User not registered in Momo
                $maintitle="Maximum amount";
                $title="Maximum amount";
                $response="Sorry the maximum amount to send is 2,000,000Frw";
                //return redirect()->route('balance')->with($title,$message);
//                    echo "$response";
                return back()->with('success',$response);
            }
            else{
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_POSTFIELDS => "",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: akokanya.com",
                        "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                        "User-Agent: PostmanRuntime/7.11.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache",
                        "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                        "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                    ),
                ));

                $responseapis = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    //echo $response;
                    $responsedata=json_decode($responseapis);
                    $deletedat=0;

                    $Momopayment = momotransaction::create([
                        'phone'=> $request['payment_number'],
                        'user_id'=> Auth()->user()->id,
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        'amount'=> $amount,
                        'domain_purchases_id'=> $id,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,
                    ]);
                    if($Momopayment){
                        $create_pendind_domain = new RenewDomains();
                        $create_pendind_domain->domain_name = $domainname;
                        $create_pendind_domain->domain_id = $id;
                        $create_pendind_domain->domain_period = $period;
                        $create_pendind_domain->domain_exp = $updatedexp;
                        $create_pendind_domain->user_id = Auth::id();
                        $create_pendind_domain->save();
                        return back()->with('success','Domain will be renewed after one minute once your transactions is successfully approved, Dial *182*7# and follow rules.');
                    }
                }
            }
        }

    }
}
