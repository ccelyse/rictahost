<?php

namespace App\Http\Controllers;

use App\Countries;
use App\DomainPurchases;
use App\DomainTransactions;
//use App\Mail\OrderPlaced;
use App\momotransaction;
use App\Order;
use App\SearchedDomain;
//use Gloudemans\Shoppingcart\Cart;
use App\SongVotes;
use App\Votes;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Metaregistrar\EPP\eppCheckDomainRequest;
use Metaregistrar\EPP\eppConnection;

class FrontendController extends Controller
{
    public function SearchDomain(Request $request){
        $all = $request->all();
        $domains = $request['domains'];
        $last3chars = substr($domains['0'], -3);
        if($last3chars == ".rw"){
            try {
                // Please enter your own settings file here under before using this example
                if ($conn = eppConnection::create(base_path(). '/settings.ini')) {
                    $conn->setLogFile(base_path(). '/epp.log');
                    // Connect to the EPP server
                    if ($conn->login()) {
                        // Create request to be sent to EPP service
                        $check = new eppCheckDomainRequest($domains);
//                    dd($check);
                        // Write request to EPP service, read and check the results
                        $response = $conn->request($check);
                        if ($response) {
                            // Walk through the results
                            $checks = $response->getCheckedDomains();
                            foreach ($checks as $check) {
                                $domain = $check['domainname'];
                                $domain_status = $check['available'];
                                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                $pin = mt_rand(1000000, 9999999)
                                    . mt_rand(1000000, 9999999)
                                    . $characters[rand(0, strlen($characters) - 1)];
                                $string = str_shuffle($pin);
                                $extension = substr($domain, strpos($domain, ".") + 1);
                                $savedomain = new SearchedDomain();
                                $savedomain->user_id = Auth::id();
                                $savedomain->domain_name = $domain;
                                $savedomain->domain_name_code = $string;
                                $savedomain->save();

                                if($extension == "rw"){
                                    $domain_price = "13000";
                                }else{
                                    $domain_price = "6000";
                                }
                            }
                            return view('frontend.SearchDomain')->with(['string'=>$string,'domain'=>$domain,'domain_status'=>$domain_status,'domain_price'=>$domain_price]);
                        }
                    }
                }
            } catch (eppException $e) {
                echo $e->getMessage();
            }
        }else{
          $doman_extension = ".rw";
          $newdomain = $domains['0'].$doman_extension;
          $arraydomain = array([$newdomain]);
//          dd($arraydomain['0']);
            try {
                // Please enter your own settings file here under before using this example
                if ($conn = eppConnection::create(base_path(). '/settings.ini')) {
                    $conn->setLogFile(base_path(). '/epp.log');
                    // Connect to the EPP server
                    if ($conn->login()) {
                        // Create request to be sent to EPP service
                        $check = new eppCheckDomainRequest($arraydomain['0']);
//                    dd($check);
                        // Write request to EPP service, read and check the results
                        $response = $conn->request($check);
                        if ($response) {
                            // Walk through the results
                            $checks = $response->getCheckedDomains();
                            foreach ($checks as $check) {
                                $domain = $check['domainname'];
                                $domain_status = $check['available'];
                                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                $pin = mt_rand(1000000, 9999999)
                                    . mt_rand(1000000, 9999999)
                                    . $characters[rand(0, strlen($characters) - 1)];
                                $string = str_shuffle($pin);
                                $extension = substr($domain, strpos($domain, ".") + 1);
                                $savedomain = new SearchedDomain();
                                $savedomain->user_id = Auth::id();
                                $savedomain->domain_name = $domain;
                                $savedomain->domain_name_code = $string;
                                $savedomain->save();

                                if($extension == "rw"){
                                    $domain_price = "13000";
                                }else{
                                    $domain_price = "6000";
                                }
                            }
                            return view('frontend.SearchDomain')->with(['string'=>$string,'domain'=>$domain,'domain_status'=>$domain_status,'domain_price'=>$domain_price]);
                        }
                    }
                }
            } catch (eppException $e) {
                echo $e->getMessage();
            }
        }


//
    }
    public function Domains(){
        return view('frontend.Domains');
    }
    public function CartDomain(){
        $list = Countries::all();
        return view('frontend.CartDomain')->with(['list'=>$list]);
    }
    public function AddToCart(Request $request){
        $all = $request->all();
        $id = $request->id;
        $price = $request->price;
        $name = $request->name;
        $quantity = $request->quantity;

        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->name === $request->name;
        });

        if (!$duplicates->isEmpty()) {
            return redirect('Domains')->with('success','Item is already in your cart!');
        }

        Cart::add($request->id, $request->name, 1, $request->price)->associate('App\NewProduct');

//        dd(Cart::content());

        return redirect('Domains')->with('success','Your item has been to added to cart');

    }
    public function DeleteProduct(Request $request){
        $id = $request['id'];
        Cart::remove($id);
        return redirect()->back()->with('success','Your item has been removed');
    }
    public function PurchaseDomain(Request $request){
        $all = $request->all();
        $dateexp = Carbon::now();
        $setdate = $dateexp->addYear();
        $str = '250';
        $str = ltrim($request['billing_phone'], ':');
        $phone_code = '+250.';
        $newphone = $phone_code.$str;

        foreach(Cart::content() as $item){
            $order = DomainPurchases::create([
                'user_id' => auth()->user()->id,
                'domain_name' => $item->name,
                'domain_id' => $item->id,
                'ns1' => 'NS949.WEBSITEWELCOME.COM',
                'ns2' => 'NS950.WEBSITEWELCOME.COM',
                'ns3' => null,
                'ns4' => null,
                'reg_period' => $item->qty,
                'exp_date' => $setdate,
                'subtotal' => $item->subtotal,
                'tax' => $item->tax,
                'total' => $item->total,
                'price' => $item->total,
                'order_status' => 'pending',
                'error' => 'null'
            ]);

            $last_id = $order->id;

            $domain_trans = new DomainTransactions();
            $domain_trans->payment_number = $request['payment_number'];
            $domain_trans->billing_names = $request['billing_names'];
            $domain_trans->billing_email = $request['billing_email'];
            $domain_trans->billing_phone = $request['billing_phone'];
            $domain_trans->billing_organisation = $request['billing_organisation'];
            $domain_trans->billing_address = $request['billing_address'];
            $domain_trans->billing_postal = $request['billing_postal'];
            $domain_trans->billing_city = $request['billing_city'];
            $domain_trans->billing_country = $request['billing_country'];
            $domain_trans->domain_purchases_id = $last_id;
            $domain_trans->save();

            $domainname = $item->name;
            $lastdomaintrans = $domain_trans->id;

//            $newstr = '25';
//            $newstr = ltrim($request['billing_phone'], ':');
//            dd($phonenumber);
//            $str_number = substr($newstr, 1);
            $phonenumber = $request['payment_number'];
            $amount = $item->total;
//            $amount = "50";
            $idt = mt_rand(10, 99);
            //Generating Payment Gateway
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=SDHost&payment_code=$idt",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "content-length: "
                ),
            ));

            $responseapi = curl_exec($curl);
//            dd($response);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                //echo "cURL Error #:" . $err;
                $title="Not Connected";
                $message="Sorry you do not have no internet connection";
                $response = "Sorry you do not have no internet connection";
                //return redirect()->route('balance')->with($title,$message);
                echo "$err";
                return view('frontend.ApproveMMError')->with(['response'=>$response]);

            } else {
                //echo $response;

                //Showing Status
                //second part Status checking

                $responseid=json_decode($responseapi);
//                        dd($responseid);

                $curl = curl_init();

                if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                    //checking balance
                    $maintitle="Balance";
                    $title="Not Enough Balance";
                    $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                    //return redirect()->route('balance')->with($title,$message);
//                    echo "$response";
                    return view('frontend.ApproveMMError')->with(['response'=>$response]);
                }
                elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                    //User not registered in Momo
                    $maintitle="Not Registered";
                    $title="Not Registered";
                    $response="Sorry you're not registered with MTN Mobile Rwanda";
//                    echo "$response";
                return view('frontend.ApproveMMError')->with(['response'=>$response]);
                }
                elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                    //User not registered in Momo
                    $maintitle="Minimum amount";
                    $title="Minimum amount";
                    $response="Sorry the minimum amount to send is 100Frw";
//                    echo "$response";
                    return view('frontend.ApproveMMError')->with(['response'=>$response]);
                }
                elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                    //User not registered in Momo
                    $maintitle="Maximum amount";
                    $title="Maximum amount";
                    $response="Sorry the maximum amount to send is 2,000,000Frw";
                    //return redirect()->route('balance')->with($title,$message);
//                    echo "$response";
                return view('frontend.ApproveMMError')->with(['response'=>$response]);
                }
                else{
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_POSTFIELDS => "",
                        CURLOPT_HTTPHEADER => array(
                            "Accept: */*",
                            "Cache-Control: no-cache",
                            "Connection: keep-alive",
                            "Host: akokanya.com",
                            "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                            "User-Agent: PostmanRuntime/7.11.0",
                            "accept-encoding: gzip, deflate",
                            "cache-control: no-cache",
                            "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                            "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                        ),
                    ));

                    $responseapis = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        echo "cURL Error #:" . $err;
                    } else {
                        //echo $response;
                        $responsedata=json_decode($responseapis);
                        $deletedat=0;

                        $Momopayment = momotransaction::create([
                            'phone'=> $request['payment_number'],
                            'user_id'=> Auth()->user()->id,
                            'transactionid'=> $responsedata[0]->external_payment_code,
                            'status'=> $responsedata[0]->payment_status,
                            'assignedid'=> $responsedata[0]->id,
                            'company_name'=> $responsedata[0]->company_name,
                            'code'=> $responsedata[0]->code,
                            'amount'=> $amount,
                            'domain_purchases_id'=> $lastdomaintrans,
                            'payment_code'=> $responsedata[0]->payment_code,
                            'external_payment_code'=> $responsedata[0]->external_payment_code,
                            'payment_status'=> $responsedata[0]->payment_status,
                            'payment_type'=> $responsedata[0]->payment_type,
                            'callback_url'=> $responsedata[0]->callback_url,
                            'momodeleted_at'=> $deletedat,
                            'momocreated_at'=> $responsedata[0]->created_at,
                            'momoupdated_at'=> $responsedata[0]->updated_at,
                        ]);
                        if($Momopayment){
//                            echo "success";
                        return view('frontend.ApproveMM');
                        }
                    }
                }
            }

//            try {
//                // Please enter your own settings file here under before using this example
//                if ($conn = eppConnection::create(base_path(). '/settings.ini')) {
//                    $conn->setLogFile(base_path(). '/epp.log');
//                    if ($conn->login()) {
//                        $nameservers = array('NS949.WEBSITEWELCOME.COM','NS950.WEBSITEWELCOME.COM');
//
//                        $email = $request['billing_email'];
//                        $phone =    $newphone;
//                        $name = $request['billing_names'];
//                        $organization = $request['billing_organisation'];
//                        $address = $request['billing_address'];
//                        $postcode = $request['billing_postal'];
//                        $city = $request['billing_city'];
//                        $country = $request['billing_country'];
//
//                        $contactid = createcontact($conn, $email, $phone, $name, $organization, $address, $postcode, $city, $country);
//                        if ($contactid) {
//                            $domaincreated = createdomain($conn, $domainname, $contactid, $contactid, $contactid, $contactid, $nameservers);
//                        }
//                        $conn->logout();
//                    }
//                }
//            } catch (eppException $e) {
//                echo $e->getMessage();
//            }
        }
//
    }
    public function VoteNowNumber(){

        $stat='PENDING';

        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();

        $transactions = MomoTransaction::get()->count();
        $transacs = MomoTransaction::where('status','like','%'.$stat.'%')
//            ->whereBetween('created_at', [$yesterday,$tomorrow])
            ->get();
        //Updateloop
        $num = count((array)$transactions);

        $nums = array(count((array)$transactions)=>$num);

        //dd($transacs);
        //return view('backend.dashboard')->with(['num'=>$num]);

        foreach($transacs as $transac){

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;

                $responsedata=json_decode($response);

                //Update to database Momo payments
                //echo $responsedata[0]->payment_status;

                $deletedat=0;

                $Momopayment = MomoTransaction::where('transactionid',$responsedata[0]->external_payment_code)
                    ->update([
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,

                    ]);

                if($Momopayment){
                    $newstatus = $responsedata[0]->payment_status;

                    if($newstatus == "SUCCESSFUL"){
                        $updateverstatus = DomainPurchases::where('id',$transac->domain_purchases_id)
                            ->update(['order_status'=> $responsedata[0]->payment_status]);
                        $billing = DomainTransactions::where('domain_purchases_id',$transac->domain_purchases_id)->get();

                        foreach ($billing as $datas){
                            $email = $datas->billing_email;
                            $phone = $datas->billing_phone;
                            $names = $datas->billing_names;
                            $organization = $datas->billing_organisation;
                            $address = $datas->billing_address;
                            $postal = $datas->billing_postal;
                            $city = $datas->billing_city;
                            $country = $datas->billing_country;
                        }

                        $str_number = substr($phone, 3);
                        $phone_code = '+250.';
                        $newphone = $phone_code.$str_number;
                        $domainname = DomainPurchases::where('id',$transac->domain_purchases_id)->value('domain_name');

                        try {
                            // Please enter your own settings file here under before using this example
                            if ($conn = eppConnection::create(base_path(). '/settings.ini')) {
                                $conn->setLogFile(base_path(). '/epp.log');
                                if ($conn->login()) {
                                    $nameservers = array('NS949.WEBSITEWELCOME.COM','NS950.WEBSITEWELCOME.COM');

                                    $email = $email;
                                    $phone =    $newphone;
                                    $name = $names;
                                    $organization = $organization;
                                    $address = $address;
                                    $postcode = $postal;
                                    $city = $city;
                                    $country = $country;

                                    $contactid = createcontact($conn, $email, $phone, $name, $organization, $address, $postcode, $city, $country);
                                    if ($contactid) {
                                        $domaincreated = createdomain($conn, $domainname, $contactid, $contactid, $contactid, $contactid, $nameservers);
                                    }
                                    $conn->logout();
                                }
                            }
                        } catch (eppException $e) {
                            echo $e->getMessage();
                        }

                    }else{

                        $updateverstatus = DomainPurchases::where('id',$transac->domain_purchases_id)
                            ->update(['order_status'=> $responsedata[0]->payment_status]);
                    }
//                    echo "Every minute request has been executed";
                    $this->info('Every minute request has been executed');
                }else{
//                    echo "Every minute request has been executed but status has not changed";
                    $this->info('Every minute request has been executed but status has not changed');
                }

                //end Update to database Momo payments
            }
            //End Status showing
        }
//        echo "Every minute request has been executed";
//        $this->info('Every minute request has been executed');
    }
    public function ApproveMM(){
        return view('frontend.ApproveMM');
    }
    public function ApproveMMError(){
        return view('frontend.ApproveMMError');
    }

}
