<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomainTransactions extends Model
{
    protected $table = "domain_transactions";
    protected $fillable = ['id','payment_number','billing_names','billing_email','billing_phone','billing_organisation',
        'billing_address','billing_postal','billing_city','billing_country','domain_purchases_id'];
}
