<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RenewDomains extends Model
{
    protected $table = "renew_domains";
    protected $fillable = ['id','user_id','domain_name','domain_id','domain_period','domain_exp'];

}
