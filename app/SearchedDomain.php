<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchedDomain extends Model
{
    protected $table = "searched_domains";
    protected $fillable = ['id','user_id','domain_name','domain_name_code'];
}
