<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class momotransaction extends Model
{
    protected $table = "momotransactions";
    protected $fillable = ['phone','user_id','transactionid', 'status', 'assignedid', 'company_name', 'code', 'amount','domain_purchases_id', 'payment_code',
        'external_payment_code', 'payment_status', 'payment_type', 'callback_url', 'momodeleted_at', 'momocreated_at', 'momoupdated_at',
    ];
}
