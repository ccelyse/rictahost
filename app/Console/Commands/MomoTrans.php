<?php

namespace App\Console\Commands;

use App\DomainPurchases;
use App\DomainTransactions;
use App\momotransaction;
use App\Votes;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Metaregistrar\EPP\eppConnection;

class MomoTrans extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Momo:checkstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking Momo Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $stat='PENDING';

        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();

        $transactions = momotransaction::get()->count();
        $transacs = momotransaction::where('status','like','%'.$stat.'%')
//            ->whereBetween('created_at', [$yesterday,$tomorrow])
            ->get();
        //Updateloop
        $num = count((array)$transactions);

        $nums = array(count((array)$transactions)=>$num);

        foreach($transacs as $transac){

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;

                $responsedata=json_decode($response);

                $deletedat=0;

                $Momopayment = momotransaction::where('transactionid',$responsedata[0]->external_payment_code)
                    ->update([
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,

                    ]);

                if($Momopayment){
                    $newstatus = $responsedata[0]->payment_status;

                    if($newstatus == "SUCCESSFUL"){
                        $updateverstatus = DomainPurchases::where('id',$transac->domain_purchases_id)
                            ->update(['order_status'=> $responsedata[0]->payment_status]);
                        $billing = DomainTransactions::where('domain_purchases_id',$transac->domain_purchases_id)->get();

                        foreach ($billing as $datas){
                            $email = $datas->billing_email;
                            $phone = $datas->billing_phone;
                            $names = $datas->billing_names;
                            $organization = $datas->billing_organisation;
                            $address = $datas->billing_address;
                            $postal = $datas->billing_postal;
                            $city = $datas->billing_city;
                            $country = $datas->billing_country;
                        }

                        $str_number = substr($phone, 3);
                        $phone_code = '+250.';
                        $newphone = $phone_code.$str_number;
                        $domainname = DomainPurchases::where('id',$transac->domain_purchases_id)->value('domain_name');

                        try {
                            // Please enter your own settings file here under before using this example
                            if ($conn = eppConnection::create(base_path(). '/settings.ini')) {
                                $conn->setLogFile(base_path(). '/epp.log');
                                if ($conn->login()) {
                                    $nameservers = array('NS949.WEBSITEWELCOME.COM','NS950.WEBSITEWELCOME.COM');

                                    $email = $email;
                                    $phone =    $newphone;
                                    $name = $names;
                                    $organization = $organization;
                                    $address = $address;
                                    $postcode = $postal;
                                    $city = $city;
                                    $country = $country;

                                    $contactid = createcontact($conn, $email, $phone, $name, $organization, $address, $postcode, $city, $country);
                                    if ($contactid) {
                                        $domaincreated = createdomain($conn, $domainname, $contactid, $contactid, $contactid, $contactid, $nameservers);
                                    }
                                    $conn->logout();
                                }
                            }
                        } catch (eppException $e) {
                            echo $e->getMessage();
                        }

                    }else{

                        $updateverstatus = DomainPurchases::where('id',$transac->domain_purchases_id)
                            ->update(['order_status'=> $responsedata[0]->payment_status]);
                    }
//                    echo "Every minute request has been executed";
                    $this->info('Every minute request has been executed');
                }else{
//                    echo "Every minute request has been executed but status has not changed";
                    $this->info('Every minute request has been executed but status has not changed');
                }

                //end Update to database Momo payments
            }
            //End Status showing
        }
    }

}
