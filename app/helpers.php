<?php

use App\DomainPurchases;
use Illuminate\Support\Facades\Auth;
use Metaregistrar\EPP\eppConnection;
use Metaregistrar\EPP\eppException;
use Metaregistrar\EPP\EppCheckException;

use Metaregistrar\EPP\eppContactHandle;
use Metaregistrar\EPP\eppCheckRequest;
use Metaregistrar\EPP\eppContactPostalInfo;
use Metaregistrar\EPP\eppContact;
use Metaregistrar\EPP\eppCreateContactRequest;

use Metaregistrar\EPP\eppCheckHostRequest;
use Metaregistrar\EPP\eppHost;
use Metaregistrar\EPP\eppCreateHostRequest;

use Metaregistrar\EPP\eppDomain;
use Metaregistrar\EPP\eppCreateDomainRequest;
use Metaregistrar\EPP\eppRenewRequest;
use Metaregistrar\EPP\eppRenewResponse;

use Metaregistrar\EPP\eppInfoDomainRequest;
use Metaregistrar\EPP\eppInfoDomainResponse;

use Metaregistrar\EPP\eppCheckDomainRequest;
use Metaregistrar\EPP\eppCheckDomainResponse;

use Metaregistrar\EPP\eppUpdateDomainRequest;
use Metaregistrar\EPP\eppUpdateDomainResponse;

use Metaregistrar\EPP\sidnEppException;
use Metaregistrar\EPP\sidnEppInfoDomainRequest;
use Metaregistrar\EPP\sidnEppInfoDomainResponse;

use Metaregistrar\EPP\eppTransferRequest;

use Gloudemans\Shoppingcart\Facades\Cart;

function getUserFullName($id) {
    $data = \DB::table('users')
        ->select(\DB::raw('CONCAT(name) as username'))
        ->where('id', $id)
        ->first();

    return $data->username;
}

function getHostingPrice($id){
    $data = \DB::table('hostings')
        ->select(\DB::raw('price'))
        ->where('id', $id)
        ->first();

    return $data->price;
}

// Domain Availability Checking
function DomainFormating($domain){
    $domain = explode('/', $domain);
    $domain = trim($domain[0]);
    $domain = explode('.', $domain);
    //dd($domain[0]);
    return $domain[0];

}

function whois_query($domain) {

    // Check and tidy domain name

    $domain = strtolower(trim($domain));
    $domain = preg_replace('/^http:\/\//i', '', $domain);
    $domain = preg_replace('/^www\./i', '', $domain);
    $domain = explode('/', $domain);
    $domain = trim($domain[0]);
    $dp = explode(".", $domain);
    $dp = $dp[1];

    // Error checking
    $arrErrors = array();


    // Extension array (populated from database in InterNoetics origial version. Here for simplicity.)
    $available = array("ac", "rw", "co.rw", "org.rw", "com", "net","org","info","biz");

    if (!in_array("$dp", $available)) $arrErrors['unav'] = 'Domain extension either invalid or unavailable.';

    // If errors, we'll output each element of the array.

    if (count($arrErrors) != 0) {

        $strError = '<img src="images/error.gif" width="16" height="16" hspace="5" alt="">Please check the following errors:<ul>';

        foreach ($arrErrors as $error) {
            $strError .= "<li>$error</li>";
        }

        $strError .= '</ul>';

        // Dodgy display area for WhoIs information.

        echo "<div id=\"container\"><div id=\"content\">";
        echo "$strError<br/><br/><br/><a href=\"javascript:history.back()\">Go Back</a>";
        echo "</div></div>";

        exit;

    }

    $_domain = explode('.', $domain);
    $lst = count($_domain)-1;
    $ext = $_domain[$lst];

    // Whois servers - text file maintained at Internoetics.

    $servers = array(
        "ac" => "whois.nic.ac",
        "rw" => "whois.ricta.org.rw",
        "co.rw" => "whois.ricta.org.rw",
        "org.rw" => "whois.ricta.org.rw",
        "com" => "whois.crsnic.net",
        "net" => "whois.crsnic.net",
        "org" => "whois.publicinterestregistry.net",
        "info" => "whois.afilias.net",
        "biz" => "whois.neulevel.biz"
    );

    // Connect to server and get whois information

    if (isset($servers[$ext])) {
        $nic_server = $servers[$ext];
        $output = '';
        // connect to whois server:
        if ($conn = fsockopen ($nic_server, 43)) {
            fputs($conn, $domain."\r\n");
            while(!feof($conn)) {
                $output .= fgets($conn,128);
            }
            fclose($conn);
            return $domain.' is not available';
        }


        else { die('Error: Could not connect to ' . $nic_server . '!'); }
        //echo "<div id=\"whoiscontent\" align=\"center\">";
        //echo "<a href=\"javascript:history.back()\">Go Back</a><br><br>";
        //return $output;
        //echo "</div><br><br><br>";
        return $domain.' is available';

    }

}

function createcontact($conn, $email, $telephone, $name, $organization, $address, $postcode, $city, $country) {
    /* @var $conn Metaregistrar\EPP\eppConnection */
    try {
        $contactinfo = new eppContact(new eppContactPostalInfo($name, $city, $country, $organization, $address, null, $postcode, Metaregistrar\EPP\eppContact::TYPE_LOC), $email, $telephone);
        $contactinfo->setPassword('fubar');
        $contact = new eppCreateContactRequest($contactinfo);
        if ($response = $conn->request($contact)) {
            /* @var $response Metaregistrar\EPP\eppCreateContactResponse */
            //echo "Contact created on " . $response->getContactCreateDate() . " with id " . $response->getContactId() . "\n";
            return $response->getContactId();
        }
    } catch (eppException $e) {
        echo $e->getMessage() . "\n";
    }
    return null;
}

function checkhosts($conn, $hosts) {
    // Create request to be sent to EPP service
    $check = new eppCheckHostRequest($hosts);
    // Write request to EPP service, read and check the results
    if ($response = $conn->request($check)) {
        /* @var $response Metaregistrar\EPP\eppCheckHostResponse */
        // Walk through the results
        $checks = $response->getCheckedHosts();
        foreach ($checks as $hostname=>$check) {
            echo $hostname . " is " . ($check ? 'free' : 'taken')."\n";
        }
    }
}

function createhost($conn, $hostname, $ipaddress=null) {
    try {
        $create = new eppCreateHostRequest(new eppHost($hostname,$ipaddress));
        if ($response = $conn->request($create)) {
            /* @var $response Metaregistrar\EPP\eppCreateHostResponse */
            echo "Host created on " . $response->getHostCreateDate() . " with name " . $response->getHostName() . "\n";
        }
    } catch (eppException $e) {
        echo $e->getMessage() . "\n";
    }
}

function createdomain($conn, $domainname, $registrant, $admincontact, $techcontact, $billingcontact, $nameservers) {
    /* @var $conn Metaregistrar\EPP\eppConnection */
    try {
        $domain = new eppDomain($domainname, $registrant);
        $domain->setRegistrant(new eppContactHandle($registrant));
        $domain->addContact(new eppContactHandle($admincontact, eppContactHandle::CONTACT_TYPE_ADMIN));
        $domain->addContact(new eppContactHandle($techcontact, eppContactHandle::CONTACT_TYPE_TECH));
        $domain->addContact(new eppContactHandle($billingcontact, eppContactHandle::CONTACT_TYPE_BILLING));
        $domain->setAuthorisationCode('rand0m');
        if (is_array($nameservers)) {
            foreach ($nameservers as $nameserver) {
                $domain->addHost(new eppHost($nameserver));
            }
        }
        $create = new eppCreateDomainRequest($domain);
        if ($response = $conn->request($create)) {
            /* @var $response Metaregistrar\EPP\eppCreateDomainResponse */
            //echo "Domain " . $response->getDomainName() . " created on " . $response->getDomainCreateDate() . ", expiration date is " . $response->getDomainExpirationDate() . "\n";
            //return view('front.cart.thankyou')->with('Thank you, Your payment has been accepted successfully.');


            Cart::instance('default')->destroy();
            //echo 'Thank you, Your order has been paid successfully.';
            $view = (string)View::make('frontend.Domains');
            echo $view;
//            return redirect('Domains')->with('success','Your order has been placed');
            //return view('front.cart.thankyou')->with('success', 'Your payment has been successfully accepted.');
        }
    } catch (eppException $e) {
        echo $e->getMessage() . "\n";
    }
}

function checkdomains($conn, $domains) {
    // Create request to be sent to EPP service
    $check = new eppCheckDomainRequest($domains);
    // Write request to EPP service, read and check the results
    $response = $conn->request($check);
    if ($response) {
        /* @var $response eppCheckDomainResponse */
        // Walk through the results
        $checks = $response->getCheckedDomains();
        foreach ($checks as $check) {
            echo $check['domainname'] . " is " . ($check['available'] ? 'free<br>' : 'taken<br>');
            if ($check['available']) {
                echo ' (' . $check['reason'] .')';
            }
            echo "\n";
        }
    }
}

//function renewdomain($conn, $domainname) {
//    // Create request to be sent to EPP service
//    $domain = new eppDomain($domainname);
//    $info = new eppInfoDomainRequest($domain);
//
//    if ($response = $conn->request($info)) {
//        dd($response);
//        /* @var $response eppInfoDomainResponse */
//        $expdate = date('Y-m-d',strtotime($response->getDomainExpirationDate()));
//        $renew = new eppRenewRequest($domain,$expdate);
//        // Write request to EPP service, read and check the results
//        if ($response = $conn->request($renew)) {
//            /* @var $response eppRenewResponse */
//            if ($response->getResultCode()==1000) {
//                return true;
//            }
//        }
//    }
//    return false;
//}
function renewdomain($conn, $domainname,$updatedexp,$period,$id) {
    // Create request to be sent to EPP service
//    $expdate = $updatedexp;
    $domain = new eppDomain($domainname);
    $info = new eppInfoDomainRequest($domain);

    if ($response = $conn->request($info)) {
        /* @var $response eppInfoDomainResponse */
        $expdate = date('Y-m-d',strtotime($response->getDomainExpirationDate()));
        $renew = new eppRenewRequest($domain,$expdate);
        $xml ='<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
        <command>
            <renew>
            <domain:renew
            xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
                <domain:name>'.$domainname.'</domain:name>
                <domain:curExpDate>'.$expdate.'</domain:curExpDate>
                <domain:period unit="y">'.$period.'</domain:period>
            </domain:renew>
            </renew>
            <clTRID>ABC-12345</clTRID>
        </command>
        </epp>';

        $update = DomainPurchases::where('id',$id)->update(['exp_date'=>$updatedexp,'reg_period'=>$period]);
        $deletepending = \App\RenewDomains::where('domain_name',$domainname)->delete();

        if($conn->write($xml)) {
            $readcounter = 0;
            $xml = $conn->read();
            // When no data is present on the stream, retry reading several times
            while ((strlen($xml)==0) && ($readcounter < $this->retry)) {
                $xml = $this->read();
                $readcounter++;
            }
        }

        /* @var $response eppRenewResponse */
        if ($response->getResultCode()==1000) {
            return true;
        }

    }
    return false;
}

function infodomain($conn, $domainname) {
    $info = new eppInfoDomainRequest(new eppDomain($domainname));

    if ($response = $conn->request($info)) {
        /* @var $response Metaregistrar\EPP\eppInfoDomainResponse */
        $d = $response->getDomain();
        //dd($d);
        echo "Info domain for " . $d->getDomainname() . ":\n";
        echo "Created on " . $response->getDomainCreateDate() . "\n";
        echo "Last update on ".$response->getDomainUpdateDate()."\n";
        echo "Registrant " . $d->getRegistrant() . "\n";
        echo "Contact info:\n";
        foreach ($d->getContacts() as $contact) {
            /* @var $contact eppContactHandle */
            echo "  " . $contact->getContactType() . ": " . $contact->getContactHandle() . "\n";
        }
        echo "Nameserver info:\n";
        foreach ($d->getHosts() as $nameserver) {
            /* @var $nameserver eppHost */
            echo "  " . $nameserver->getHostname() . "\n";
        }
    } else {
        echo "ERROR2\n";
    }
    return null;
}
// function infodomain($domainname) {
//     $info = new eppInfoDomainRequest(new eppDomain($domainname));
//         try{
//             $response = $this->conn->request($info);
//         }
//         catch(\Exception $e){
//             return null;
//         }
//         if ($response) {
//             /* @var $response Metaregistrar\EPP\eppInfoDomainResponse */
//             $d = $response->getDomain();

//             $contacts = [];
//             foreach ($d->getContacts() as $contact) {
//                 /* @var $contact eppContactHandle */
//                 $contacts[$contact->getContactType()] = $contact->getContactHandle();
//             }

//             $nameservers = [];
//             foreach ($d->getHosts() as $nameserver) {
//                 /* @var $nameserver eppHost */
//                 $statusses = [];
//                 $HostStatusses = $nameserver->getHostStatuses();
//                 if($HostStatusses!=NULL){
//                     foreach($HostStatusses as $status){
//                         $statusses[] = $status;
//                     }
//                 }else{
//                     $statusses[] = 'active';
//                 }

//                 $nameservers[] = [
//                     'ldhName'   =>  $nameserver->getHostname(),
//                     'objectClassName'   =>  "nameserver",
//                     'status'    =>  $statusses,
//                     'events'    =>  [
//                         [
//                             'eventAction'   =>  'last changed',
//                             'eventDate' =>  $response->getDomainUpdateDate()
//                         ]
//                     ]
//                 ];
//             }


//             #   Domain Array
//             return  [
//                 'domainHandle'    =>  $d->getDomainId(),
//                 'domain'    =>  $d->getDomainname(),
//                 'created'    =>  $response->getDomainCreateDate(),
//                 'last_update'    =>  $response->getDomainUpdateDate(),
//                 'expire'        =>  $response->getDomainExpirationDate(),
//                 'registrant'    =>  $d->getRegistrant(),
//                 'contacts'    =>  $contacts,
//                 'nameservers'    =>  $nameservers,
//                 'status'        =>  $response->getDomainStatuses(),
//                 'registrar'        =>  $response->getDomainClientId()
//             ];


//         } else {
//             echo "ERROR2\n";
//         }
//     return null;
// }


function getDomainInfo($conn,$domain)
{
    try {
        $eppDomain = new sidnEppInfoDomainRequest(new eppDomain($domain));
        /** @var sidnEppInfoDomainResponse $res */
        //$res = new epp();
        if ($res = $conn->request($eppDomain)) {
            return $res;
        }
        return false;
    } catch (eppException $e) {
        throw new eppException($e->getMessage(), $e->getCode());
    }
}



function modifydomain($conn, $domainname, $registrant = null, $admincontact = null, $techcontact = null, $billingcontact = null, $nameservers = null,$id,$ns1,$ns2) {
    $response = null;
    try {
        // First, retrieve the current domain info. Nameservers can be unset and then set again.
        $del = null;
        $domain = new eppDomain($domainname);
        $info = new eppInfoDomainRequest($domain);

        if ($response = $conn->request($info)) {
            // If new nameservers are given, get the old ones to remove them
            if (is_array($nameservers)) {
                /* @var Metaregistrar\EPP\eppInfoDomainResponse $response */
                $oldns = $response->getDomainNameservers();
                if (is_array($oldns)) {
                    if (!$del) {
                        $del = new eppDomain($domainname);
                    }
                    foreach ($oldns as $ns) {
                        $del->addHost(new eppHost($ns->getHostname()));
                    }
                }
            }
            if ($admincontact) {
                $oldadmin = $response->getDomainContact(eppContactHandle::CONTACT_TYPE_ADMIN);
                if ($oldadmin == $admincontact) {
                    $admincontact = null;
                } else {
                    if (!$del) {
                        $del = new eppDomain($domainname);
                    }
                    $del->addContact(new eppContactHandle($oldadmin, eppContactHandle::CONTACT_TYPE_ADMIN));
                }
            }
            if ($techcontact) {
                $oldtech = $response->getDomainContact(eppContactHandle::CONTACT_TYPE_TECH);
                if ($oldtech == $techcontact) {
                    $techcontact = null;
                } else {
                    if (!$del) {
                        $del = new eppDomain($domainname);
                    }
                    $del->addContact(new eppContactHandle($oldtech, eppContactHandle::CONTACT_TYPE_TECH));
                }
            }
        }
        // In the UpdateDomain command you can set or add parameters
        // - Registrant is always set (you can only have one registrant)
        // - Admin, Tech, Billing contacts are Added (you can have multiple contacts, don't forget to remove the old ones)
        // - Nameservers are Added (you can have multiple nameservers, don't forget to remove the old ones
        $mod = null;
        if ($registrant) {
            $mod = new eppDomain($domainname);
            $mod->setRegistrant(new eppContactHandle($registrant));
        }
        $add = null;
        if ($admincontact) {
            if (!$add) {
                $add = new eppDomain($domainname);
            }
            $add->addContact(new eppContactHandle($admincontact, eppContactHandle::CONTACT_TYPE_ADMIN));
        }
        if ($techcontact) {
            if (!$add) {
                $add = new eppDomain($domainname);
            }
            $add->addContact(new eppContactHandle($techcontact, eppContactHandle::CONTACT_TYPE_TECH));
        }
        if ($billingcontact) {
            if (!$add) {
                $add = new eppDomain($domainname);
            }
            $add->addContact(new eppContactHandle($billingcontact, eppContactHandle::CONTACT_TYPE_BILLING));
        }

        if (is_array($nameservers)) {
            if (!$add) {
                $add = new eppDomain($domainname);
            }
            foreach ($nameservers as $nameserver) {
                $add->addHost(new eppHost($nameserver));
            }
        }

        $update = new eppUpdateDomainRequest($domain, $add, $del, $mod);
        //echo $update->saveXML();
        if ($response = $conn->request($update)) {
            /* @var eppUpdateDomainResponse $response */
            $update = DomainPurchases::where('id',$id)->update(['ns1'=>$ns1,
            'ns2'=>$ns2,'ns3'=>null,'ns4'=>null]);
            echo View::make('Client.DomainsManageResponse', ['domains' => DomainPurchases::where('user_id',Auth::id())
                ->where('order_status','SUCCESSFUL')->get()])->with('success','you have successfully updated nameservers');
//            echo $response->getResultMessage() . "\n";
        }
    } catch (eppException $e) {
//        echo $e->getMessage() . "\n";
        echo View::make('Client.DomainsManageResponse', ['domains' => DomainPurchases::where('user_id',Auth::id())
            ->where('order_status','SUCCESSFUL')->get(),'success'=>'your nameservers does not exit! ']);

//        if ($response instanceof eppUpdateDomainResponse) {
//            echo $response->textContent . "\n";
//        }
    }
}
function transferdomain($conn, $domainname, $authcode) {
    try {
        $domain = new eppDomain($domainname);
        $domain->setAuthorisationCode($authcode);
        $domain->setRegistrant(new \Metaregistrar\EPP\eppContactHandle('registrant handle'));
        $domain->addContact(new \Metaregistrar\EPP\eppContactHandle('admin handle', \Metaregistrar\EPP\eppContactHandle::CONTACT_TYPE_ADMIN));
        $domain->addContact(new \Metaregistrar\EPP\eppContactHandle('tech handle', \Metaregistrar\EPP\eppContactHandle::CONTACT_TYPE_TECH));
        $domain->addContact(new \Metaregistrar\EPP\eppContactHandle('billing handle', \Metaregistrar\EPP\eppContactHandle::CONTACT_TYPE_BILLING));
        $domain->addHost(new \Metaregistrar\EPP\eppHost('ns1.yourdomainprovider.net'));
        $domain->addHost(new \Metaregistrar\EPP\eppHost('ns2.yourdomainprovider.net'));
        $domain->addHost(new \Metaregistrar\EPP\eppHost('ns3.yourdomainprovider.net'));
        $transfer = new \Metaregistrar\EPP\metaregEppTransferExtendedRequest(eppTransferRequest::OPERATION_REQUEST,$domain);
        if ($response = $conn->request($transfer)) {
            /* @var $response Metaregistrar\EPP\eppTransferResponse */
            echo $response->getDomainName()," transfer request was succesful\n";
        }
    } catch (eppException $e) {
        echo $e->getMessage() . "\n";
    }
}

function getTotalDomains(){
    $domains = \DB::table('orders')
        ->where('user_id', '=', Auth::user()->id)
        ->where('service_id', '=', 2)
        ->get()
        ->count();
    return $domains;
}

function getTotalHostings(){
    $domains = \DB::table('orders')
        ->where('user_id', '=', Auth::user()->id)
        ->where('service_id', '=', 1)
        ->get()
        ->count();
    return $domains;
}

function getServiceName($id){
    $services = \DB::table('services')
        ->select('category')
        ->where('id', '=', $id)
        ->get();
    foreach($services as $service){
        return $service->category;
    }
}

function getHostingPackageName($id){
    $packages = \DB::table('hostings')
        ->select('package_name')
        ->where('id', '=', $id)
        ->get();
    foreach($packages as $package){
        return $package->package_name;
    }
}

function getDomainName($id){
    $domains = \DB::table('orders')
        ->select('domain_name')
        ->where('id', '=', $id)
        ->get();
    foreach($domains as $domain){
        return $domain->domain_name;
    }
}


