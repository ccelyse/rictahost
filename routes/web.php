<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.welcome');
});
Route::post('SearchDomain', 'FrontendController@SearchDomain');
Route::get('Domains', 'FrontendController@Domains');
Route::get('CartDomain', 'FrontendController@CartDomain');
Route::post('AddToCart', 'FrontendController@AddToCart');
Route::get('DeleteProduct', 'FrontendController@DeleteProduct');
Route::get('DeleteProduct/{id}', 'FrontendController@DeleteProduct');
Route::post('PurchaseDomain', 'FrontendController@PurchaseDomain');
Route::get('ApproveMM', 'FrontendController@ApproveMM');
Route::get('VoteNowNumber', 'FrontendController@VoteNowNumber');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('Client', 'BackendController@Client');


Route::prefix('Client')->group(function () {
    Route::get('Dashboard', 'BackendController@Dashboard');
    Route::get('SystemUser', 'BackendController@SystemUser');
    Route::post('UpdateUserDetails', 'BackendController@UpdateUserDetails');
    Route::get('DomainsManage', 'BackendController@DomainsManage');
    Route::get('DomainsManageResponse', 'BackendController@DomainsManageResponse');
    Route::post('UpdateNameServers', 'BackendController@UpdateNameServers');
    Route::post('RenewDomain', 'BackendController@RenewDomain');
    Route::post('TransferDomain', ['as'=>'TransferDomain','uses'=>'BackendController@TransferDomain']);
    Route::get('/DashboardStats', ['as'=>'DashboardStats','uses'=>'BackendController@DashboardStats']);
    Route::get('/BillingHistory', ['as'=>'BillingHistory','uses'=>'BackendController@BillingHistory']);

});

Route::prefix('Admin')->group(function () {
    Route::get('AdminDashboard', 'BackendController@AdminDashboard');
    Route::get('SystemUser', 'BackendController@SystemUser');
    Route::post('UpdateUserDetails', 'BackendController@UpdateUserDetails');
    Route::get('AdminDomainsManage', 'BackendController@AdminDomainsManage');
    Route::get('AllDomainsManage', 'BackendController@AllDomainsManage');
    Route::get('AllAccounts', 'BackendController@AllAccounts');
//    Route::get('UserDomainName/{id}', 'BackendController@UserDomainName');
    Route::get('UserDomainName',['as'=>'Admin.UserDomainName','uses'=>'BackendController@UserDomainName']);
    Route::get('DomainsManageResponse', 'BackendController@DomainsManageResponse');
    Route::post('UpdateNameServers', 'BackendController@UpdateNameServers');
    Route::post('RenewDomain', 'BackendController@RenewDomain');
    Route::post('TransferDomain', ['as'=>'TransferDomain','uses'=>'BackendController@TransferDomain']);
    Route::get('/DashboardStats', ['as'=>'DashboardStats','uses'=>'BackendController@DashboardStats']);
    Route::get('/AllBillingHistory', ['as'=>'BillingHistory','uses'=>'BackendController@AllBillingHistory']);

});