<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('domain_name');
            $table->string('domain_id');
            $table->string('ns1');
            $table->string('ns2');
            $table->string('ns3')->nullable();
            $table->string('ns4')->nullable();
            $table->string('reg_period');
            $table->string('exp_date');
            $table->string('subtotal');
            $table->string('tax');
            $table->string('price');
            $table->string('total');
            $table->string('order_status');
            $table->string('error')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_purchases');
    }
}
