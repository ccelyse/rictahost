<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMomotransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('momotransactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone');
            $table->string('user_id');
            $table->string('transactionid');
            $table->string('status');
            $table->string('assignedid');
            $table->string('company_name');
            $table->string('code');
            $table->string('amount');
            $table->unsignedInteger('domain_purchases_id');
            $table->foreign('domain_purchases_id')->references('id')->on('domain_purchases')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('payment_code');
            $table->string('external_payment_code');
            $table->string('payment_status');
            $table->string('payment_type');
            $table->string('callback_url');
            $table->string('momodeleted_at');
            $table->string('momocreated_at');
            $table->string('momoupdated_at');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('momotransactions');
    }
}
