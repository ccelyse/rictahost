<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_number');
            $table->string('billing_names');
            $table->string('billing_email');
            $table->string('billing_phone');
            $table->string('billing_organisation')->nullable();
            $table->string('billing_address');
            $table->string('billing_postal')->nullable();
            $table->string('billing_city');
            $table->string('billing_country');
            $table->unsignedInteger('domain_purchases_id');
            $table->foreign('domain_purchases_id')->references('id')->on('domain_purchases')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_transactions');
    }
}
