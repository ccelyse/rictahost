@extends('Client.layouts.master')

@section('title', 'Domains Manage | SDHost')

@section('content')

    @include('Client.layouts.sidemenu')
    @include('Client.layouts.upmenu')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }
        .btn-primary:hover{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }
        .alert-success {
            color: #fff !important;
            background-color: #0066cc !important;
            border-color: #0066cc !important;
            text-align: center !important;
            font-size: 15px !important;
            font-weight: lighter !important;
        }
    </style>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-body">
                <div class="content-body">
                    <div class="card-header" style="margin-bottom: 30px">
                        <h4 class="card-title">Billing History</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Summary</th>
                                                    <th>Transaction ID</th>
                                                    <th>Transaction Code</th>
                                                    <th>Date Paid</th>
                                                    <th>Total Amount</th>
                                                    <th>Balance</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($billings as $data)
                                                    <tr>
                                                        <td>{{$data->phone}}</td>
                                                        <td>{{$data->transactionid}}</td>
                                                        <td>{{$data->code}}</td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>{{$data->amount}} RWF</td>
                                                        <td>0 RWF</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>



                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        $(document).on('change', '#period', function() {
            var period =$('#period').val();
            var getPrice = $(this).find(':selected').attr('data-price');
            var newPrice = period * getPrice;
            $('#domain_price').val(newPrice);
        });
    </script>
    <script src="../backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="../backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="../backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="../backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="../backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>

    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <script src="../backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="../backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>

@endsection