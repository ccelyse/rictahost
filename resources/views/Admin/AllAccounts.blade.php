@extends('Client.layouts.master')

@section('title', 'Domains Manage | SDHost')

@section('content')

    @include('Client.layouts.sidemenu')
    @include('Client.layouts.upmenu')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }
        .btn-primary:hover{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }
        .alert-success {
            color: #fff !important;
            background-color: #0066cc !important;
            border-color: #0066cc !important;
            text-align: center !important;
            font-size: 15px !important;
            font-weight: lighter !important;
        }
        .card-header .heading-elements, .card-header .heading-elements-toggle {
            background-color: inherit;
            position: absolute;
            top: 33px !important;
            right: 33px !important;
        }
        .reco_find{
            margin-top: 30px;
            margin-bottom: 30px;
        }
    </style>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-body">
                <div class="content-body">
                    <div class="card-header" style="margin-bottom: 30px">
                        <h4 class="card-title">Domains Managements</h4>
{{--                        <div class="heading-elements">--}}
{{--                                <button type="button" class="btn btn-success action_btn edit-modal" data-toggle="modal" data-target="#addCategorization" style="border-radius:0px !important; margin-top: 10px; text-transform: capitalize;margin-right: 15px;">--}}
{{--                                    <i class='fas fa-globe-africa'></i> Import Domain names--}}
{{--                                </button>--}}
{{--                            <div class="modal fade" id="addCategorization" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--                                <div class="modal-dialog" role="document">--}}
{{--                                    <div class="modal-content">--}}
{{--                                        <div class="modal-header">--}}
{{--                                            --}}{{----}}
{{--                                            <h5 class="modal-title" id="exampleModalLabel">Edit Hospital / Health Center Categorization</h5>--}}
{{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                                <span aria-hidden="true">&times;</span>--}}
{{--                                            </button>--}}
{{--                                        </div>--}}
{{--                                        <div class="modal-body">--}}
{{--                                            <div class="wizard-container">--}}
{{--                                                <div class="card wizard-card" data-color="green" id="wizardProfile">--}}
{{--                                                    <form class="form-horizontal" action="#" id="AddKeyChallenges">--}}
{{--                                                        <div class='col-lg-12'>--}}
{{--                                                            <div class="form-group">--}}
{{--                                                                <label class="reco_find">Upload excel </label>--}}
{{--                                                                <input type="file" class="form-control" name="domain_names_excel">--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class='col-lg-12'>--}}
{{--                                                            <button type='button' class='btn btn-fill btn-success btn-wd' value='Update' id="savekeychallenges" style="border-radius:0px !important;margin-bottom: 15px;">Upload Excel</button>--}}
{{--                                                        </div>--}}
{{--                                                    </form>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <!-- wizard container -->--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </div>--}}
                    </div>
                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Business Name</th>
                                                    <th>Business Tin Number</th>
                                                    <th>Billing Phone</th>
                                                    <th>Billing Address</th>
                                                    <th>Billing Postal</th>
                                                    <th>Billing City</th>
                                                    <th>Billing Country</th>
                                                    <th>Billing Status</th>
                                                    <th>Role</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($accounts as $data)
                                                    <tr>
                                                        <td>
                                                            <a href="{{ route('Admin.UserDomainName',['id'=> $data->id])}}">{{$data->name}}</a>
                                                        </td>
                                                        <td>{{$data->email}}</td>
                                                        <td>{{$data->billing_business_name}}</td>
                                                        <td>{{$data->billing_tin_number}}</td>
                                                        <td>{{$data->billing_phone}}</td>
                                                        <td>{{$data->billing_address}}</td>
                                                        <td>{{$data->billing_postal}}</td>
                                                        <td>{{$data->billing_city}}</td>
                                                        <td>{{$data->billing_country}}</td>
                                                        <td>{{$data->billing_status}}</td>
                                                        <td>{{$data->role}}</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>



                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        $(document).on('change', '#period', function() {
            var period =$('#period').val();
            var getPrice = $(this).find(':selected').attr('data-price');
            var newPrice = period * getPrice;
            $('#domain_price').val(newPrice);
        });
    </script>
    <script src="../backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="../backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="../backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="../backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="../backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>

    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <script src="../backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="../backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="../backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>

@endsection