@extends('Client.layouts.master')

@section('title', 'SD Host')

@section('content')

    @include('Client.layouts.sidemenu')
    @include('Client.layouts.upmenu')
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <style>
        .height-75 {
            height: 10px!important;
        }
        .h4, h4 {
            font-size: 16px !important;
            font-weight: 400 !important;
        }
        .welcome-block {
            padding: 0;
            margin: 0 0 .5rem;
        }

        .px-4 {
            padding-right: 1.5rem!important;
            padding-left: 1.5rem!important;
        }
        .mb-5 {
            margin-bottom: 3rem!important;
        }
        normal {
            font-weight: 400!important;
        }
        .dark-text {
            color: #233647!important;
        }
        .mb-2 {
            margin-bottom: .5rem!important;
        }
    </style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

            <div id="crypto-stats-3" class="row">

                <div class="col-xl-12 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="welcome-block px-4 mb-5">
                                        <h1 class="mb-2 fw-normal dark-text">Welcome, {{ Auth::user()->name }} &#128521;</h1>
                                        <p>We think you'll like the simple, smart design of your <span style="font-weight: bold;color: #06c;"> New Customer Portal</span>.  Use the <a href="#"><strong>Click here</strong></a> to let us know how we can make it even better.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-globe-africa" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Domains</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php echo $domains;?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-server warning blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Hostings</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4>0</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-random warning blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Pending Transfers</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4>0</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-globe-africa blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Renew</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4>0</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="AllStandards" style="width:100%; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
</div>
    <script type="application/javascript">
        $(document).ready(function () {
            $.ajax({
                type: 'get',
                url: "../Client/DashboardStats",
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    JSON.stringify(data); //to string
                    console.log(data);
                    $.each(data, function (index, value) {
                        var numbers = [
                            {
                                "name": 'Domain name',
                                "data": [data.jan, data.feb, data.march, data.april,
                                    data.may,data.july, data.june, data.august, data.september,
                                    data.october, data.november, data.december,]
                            }
                        ];
                        Highcharts.chart('AllStandards', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Monthly Average Domain purchases'
                            },
                            // subtitle: {
                            //     text: 'Source: WorldClimate.com'
                            // },
                            xAxis: {
                                categories: [
                                    'Jan',
                                    'Feb',
                                    'Mar',
                                    'Apr',
                                    'May',
                                    'Jun',
                                    'Jul',
                                    'Aug',
                                    'Sep',
                                    'Oct',
                                    'Nov',
                                    'Dec'
                                ],
                                crosshair: true
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'domains'
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                            series: numbers
                        });

                    });
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
                //
            });
        })

    </script>
{{--    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
<!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection