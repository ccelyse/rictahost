@extends('Client.layouts.master')

@section('title', 'Update Account Details | SDHost')

@section('content')

    @include('Client.layouts.sidemenu')
    @include('Client.layouts.upmenu')
    <link rel="stylesheet" type="text/css" href="../backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../backend/app-assets/css/plugins/forms/wizard.css">
    <style>
        /*.app-content .wizard > .actions {*/
        /*position: relative;*/
        /*display: block;*/
        /*text-align: right;*/
        /*padding: 20px;*/
        /*padding-top: 0;*/
        /*display: none;*/
        /*}*/
        .app-content .wizard > .actions > ul > a[href='#finish'] {
            display: none !important;
        }
        .alert-success {
            color: #fff !important;
            background-color: #0066cc !important;
            border-color: #0066cc !important;
            text-align: center !important;
            font-size: 15px !important;
            font-weight: lighter !important;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
   
    
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-wrapper">
                @if (session('success'))
                    <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                        {{ session('success') }}
                    </div>
                @endif
                <div class="content-body">
                    <section id="number-tabs">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        {{--<h4 class="card-title">Form wizard with number tabs</h4>--}}
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('Client/UpdateUserDetails') }}">
                                            {{ csrf_field() }}

                                            <!-- Step 1 -->
                                                <h6 class="account_header">Account and billing information</h6>
                                                @foreach($users as $data)
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Name</label>
                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->name}}"
                                                                       name="name" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Email</label>
                                                                <input type="email" id="projectinput1" class="form-control" value="{{$data->email}}"
                                                                       name="email" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Business name</label>
                                                                <input type="text" id="projectinput1" class="form-control"
                                                                       name="billing_business_name" value="{{$data->billing_business_name}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">TIN Number</label>
                                                                <input type="text" id="projectinput1" class="form-control"
                                                                       name="billing_tin_number" value="{{$data->billing_tin_number}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Primary Phone</label>
                                                                <input id="projectinput1" class="form-control" placeholder="250782384772"
                                                                       name="billing_phone" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                       type = "number"  value="{{$data->billing_phone}}" pattern="[+]{1}[0-9]{12}" maxlength="12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Address</label>
                                                                <input type="text" id="projectinput1" class="form-control"
                                                                       name="billing_address" value="{{$data->billing_address}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Postal Code</label>
                                                                <input type="text" id="projectinput1" class="form-control"
                                                                       name="billing_postal" value="{{$data->billing_postal}}">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">City</label>
                                                                <input type="text" id="projectinput1" class="form-control"
                                                                       name="billing_city" value="{{$data->billing_city}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Country</label>
                                                                <select class="form-control"  id="village" name="billing_country" >
                                                                    <option value="{{$data->billing_country}}" selected>{{$data->country_name}}</option>
                                                                    @foreach($list as $country)
                                                                        <option value="{{$country->country_code}}">{{$country->country_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Update
                                                    </button>
                                                </div>
                                            @endforeach
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
            <div class="content-body">

            </div>
        </div>
    </div>


    <script src="../backend/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
    <script src="../backend/app-assets/js/scripts/forms/wizard-steps.js"></script>
    <script src="../backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="../backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="../backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="../backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="../backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="../backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="../backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="../backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="../backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="../backend/app-assets/js/scripts/forms/wizard-steps.js"></script>
    <script src="../backend/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>

@endsection