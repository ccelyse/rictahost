<style>
    .main-menu.menu-light .navigation>li.open>a {
        color: #545766;
        background: #f5f5f5;
        border-right: 4px solid #6b442b !important;
    }
</style>
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">

        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

{{--            <li class='nav-item'>--}}
{{--                <a href="{{url('Client/SystemUser')}}"><i class='far fa-user-circle'></i>--}}
{{--                    <span class='menu-title' data-i18n='nav.dash.main'>Account Details</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class='nav-item'>--}}
{{--                <a href="{{url('Client/DomainsManage')}}"><i class='fas fa-globe-africa'></i>--}}
{{--                    <span class='menu-title' data-i18n='nav.dash.main'>Domains Management</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class='nav-item'>--}}
{{--                <a href="#"><i class='fas fa-server'></i>--}}
{{--                    <span class='menu-title' data-i18n='nav.dash.main'>Hosting</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class='nav-item'>--}}
{{--                <a href="{{url('Domains')}}"><i class='fas fa-shopping-cart'></i>--}}
{{--                    <span class='menu-title' data-i18n='nav.dash.main'>Marketplace</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class='nav-item'>--}}
{{--                <a href="{{url('Client/BillingHistory')}}"><i class='fas fa-money-bill'></i>--}}
{{--                    <span class='menu-title' data-i18n='nav.dash.main'>Billings</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class='nav-item'>--}}
{{--                <a href="#"><i class='fas fa-phone-volume'></i>--}}
{{--                    <span class='menu-title' data-i18n='nav.dash.main'>Support</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class='nav-item'>--}}
{{--                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">--}}
{{--                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                        {{ csrf_field() }}--}}
{{--                    </form>--}}
{{--                    <i class='fas fa-power-off'></i><span class='menu-title' data-i18n='nav.dash.main'>Logout</span>--}}
{{--                </a>--}}
{{--            </li>--}}
            <?php
            $user_id = \Auth::user()->id;
            $Userrole = \App\User::where('id',$user_id)->value('role');

            $logout = route('logout');
            $BillingHistory = url('Client/BillingHistory');
            $BillingHistory_Admin = url('Admin/AllBillingHistory');
            $Domains = url('Domains');
            $DomainsManage = url('Client/DomainsManage');
            $SystemUser = url('Client/SystemUser');
            $SystemUser_Admin = url('Admin/SystemUser');
            $Dashboard = url('Dashboard');
            $AdminDashboard = url('Admin/AdminDashboard');
            $AllDomainsManage = url('Admin/AllDomainsManage');
            $AllAccounts = url('Admin/AllAccounts');
            $UserDomainName = url('Admin/UserDomainName');

            switch ($Userrole) {
                case "admin":
                    echo "<li class='nav-item'> <a href='$AdminDashboard'><span class='menu-title' data-i18n='nav.dash.main'></span></a></li>";
                    echo "<li class='nav-item'> <a href='$AdminDashboard'><i class='fas fa-tachometer-alt'></i> <span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a></li>";
                    echo "<li class='nav-item'> <a href='$SystemUser_Admin'><i class='far fa-user-circle'></i> <span class='menu-title' data-i18n='nav.dash.main'>Account Details</span></a></li>";
                    echo "<li class='nav-item'> <a href='$AllAccounts'><i class='far fa-user-circle'></i> <span class='menu-title' data-i18n='nav.dash.main'>All Accounts</span></a></li>";
                    echo "<li class='nav-item'> <a href='$AllDomainsManage'><i class='fas fa-globe-africa'></i> <span class='menu-title' data-i18n='nav.dash.main'>Domains Management</span> </a> </li>";
                    echo "<li class='nav-item'> <a href='#'><i class='fas fa-server'></i> <span class='menu-title' data-i18n='nav.dash.main'>Hosting</span> </a> </li>";
                    echo "<li class='nav-item'> <a href='$Domains'><i class='fas fa-shopping-cart'></i> <span class='menu-title' data-i18n='nav.dash.main'>Marketplace</span> </a> </li>";
                    echo "<li class='nav-item'> <a href='$BillingHistory_Admin'><i class='fas fa-money-bill'></i> <span class='menu-title' data-i18n='nav.dash.main'>Billings</span> </a> </li>";
                    echo "<li class='nav-item'> <a href=''><i class='fas fa-phone-volume'></i> <span class='menu-title' data-i18n='nav.dash.main'>Support</span> </a> </li>";
//                    echo "<li class='nav-item'> <a href='$logout' onclick='event.preventDefault();document.getElementById('logout-form').submit();'><form id='logout-form' action='$logout' method='POST' style='display: none;'>{{csrf_field()}}</form> <i class='fas fa-power-off'></i><span class='menu-title' data-i18n='nav.dash.main'>Logout</span> </a> </li>";
                    break;

                case "user":
                    echo "<li class='nav-item'> <a href='$Dashboard'><i class='fas fa-tachometer-alt'></i> <span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a></li>";
                    echo "<li class='nav-item'> <a href='$SystemUser'><i class='far fa-user-circle'></i> <span class='menu-title' data-i18n='nav.dash.main'>Account Details</span></a></li>";
//                    echo "<li class='nav-item'> <a href='$AllAccounts'><i class='far fa-user-circle'></i> <span class='menu-title' data-i18n='nav.dash.main'>All Accounts</span></a></li>";
                    echo "<li class='nav-item'> <a href='$DomainsManage'><i class='fas fa-globe-africa'></i> <span class='menu-title' data-i18n='nav.dash.main'>Domains Management</span> </a> </li>";
                    echo "<li class='nav-item'> <a href='#'><i class='fas fa-server'></i> <span class='menu-title' data-i18n='nav.dash.main'>Hosting</span> </a> </li>";
                    echo "<li class='nav-item'> <a href='$Domains'><i class='fas fa-shopping-cart'></i> <span class='menu-title' data-i18n='nav.dash.main'>Marketplace</span> </a> </li>";
                    echo "<li class='nav-item'> <a href='$BillingHistory'><i class='fas fa-money-bill'></i> <span class='menu-title' data-i18n='nav.dash.main'>Billings</span> </a> </li>";
                    echo "<li class='nav-item'> <a href=''><i class='fas fa-phone-volume'></i> <span class='menu-title' data-i18n='nav.dash.main'>Support</span> </a> </li>";
//                    echo "<li class='nav-item'> <a href='$logout' onclick='event.preventDefault();document.getElementById('logout-form').submit();'> <form id='logout-form' action='$logout' method='POST' style='display: none;'>{{csrf_field()}}</form> <i class='fas fa-power-off'></i><span class='menu-title' data-i18n='nav.dash.main'>Logout</span> </a> </li>";
                    break;
                default:
                    return redirect()->back();
                    break;
            }
            ?>
            <li class='nav-item'>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    <i class='fas fa-power-off'></i><span class='menu-title' data-i18n='nav.dash.main'>Logout</span>
                </a>
            </li>
        </ul>
    </div>
</div>


