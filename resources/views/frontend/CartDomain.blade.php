@extends('frontend.layouts.master')

@section('title', 'SD Host')

@section('content')
    <style>
        #SDHost-header {
            max-height: 420px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col {
            flex-grow: 1;
            padding: 0;
            width: 100%;
        }
        .newBrand .checkout-wrapper .checkout-items {
            background-color: #fff;
            padding: 20px 30px;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            float: left;
        }
        .ux-card {
            border: 1px solid #d6d6d6;
        }
         .product {
             color: #333;
             float: left;
             width: 100%;
         }
         .ux-card {
             border: 1px solid #e8e8e8;
             box-shadow: none;
             background: #fff;
             margin-bottom: 20px;
         }
        @media (min-width: 1200px){
            .ux-card {
                margin-bottom: 40px;
            }
        }

            .ux-card {
                border-radius: 4px;
                margin: 0 0 20px;
                color: #111;
                border: 1px solid #d4dbe0;
            }
        .product .product-info {
            padding: 15px 15px 10px;
            position: relative;
            border-bottom: 1px solid #e8e8e8;
        }
        .product .product-info .product-name-desc {
            float: left;
            max-width: 80%;
        }
        .product .name {
            font-weight: 700;
            padding-right: 10px;
        }
        .product .attr, .product .description, .product .duration, .product .free-for-now, .product .name, .product .renews {
            clear: left;
            float: left;
        }
         .product .attr, .product .description, .product .free-for-now, .product .renews, .product .termlength {
             color: #767676;
             font-size: 14px;
             line-height: 16px;
         }
         .product .description-wrap {
             max-width: 100%;
             overflow-wrap: break-word;
         }
         .newBrand .description {
             margin-bottom: 12px;
         }
        .description {
            margin-bottom: 40px;
        }
        .product .first-year-plus, .product .item-price-block, .product .price, .product .termlength {
            clear: right;
            float: right;
        }
        .product {
            color: #333;
            float: left;
            width: 100%;
        }
        .product .price {
            font-weight: 700;
        }
        .price {
            display: inline;
        }
        .text-primary-o {
            color: #09757a!important;
        }
        .product .first-year-plus {
            text-align: right;
            color: #767676;
            font-size: 12px;
            padding-bottom: 5px;
            padding-left: 5px;
        }
        .product .price {
            font-weight: 700;
        }
        .product .percent-off {
            font-size: .875em;
            padding-left: 8px;
        }
        .text-primary-o {
            color: #09757a!important;
        }
        .product .duration {
            width: 60%;
            padding: .5rem 0;
        }
        .stand-alone {
            width: 100%;
        }
        .stand-alone .duration-option {
            clear: left;
            float: left;
            width: 60%;
        }
        .stand-alone .duration-option .duration-select {
            width: 100%;
        }
        .dropdown {
            cursor: pointer;
        }
         .price {
             display: inline;
         }
        .text-primary-o {
            color: #09757a!important;
        }
        .product .attr, .product .description, .product .free-for-now, .product .renews, .product .termlength {
            color: #767676;
            font-size: 14px;
            line-height: 16px;
        }
        .product .remove {
            clear: right;
            float: right;
            min-width: inherit;
        }
        .newBrand .btn-link {
            color: #111!important;
        }
        .newBrand .add-privacy-crossell {
            border-top: 1px solid #d6d6d6;
            padding: 0;
        }
        .add-privacy-crossell, .display-privacy-crossell {
            /*border-top: 1px solid #e8e8e8;*/
        }
        .privacy-crosssell {
            clear: both;
            padding: 15px;
        }
            .add-privacy-container {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                align-items: stretch;
            }

        .arrow-div {
            border-radius: 0;
            margin-right: 0;
        }
        @media (min-width: 768px) {
            .arrow-div {
                display: flex;
                flex-direction: column;
                justify-content: center;
                margin-right: 15px;
                /*max-width: 215px;*/
                position: relative;
            }
        }
         .arrow-div {
             background: #ffeea9;
             border-radius: 8px;
             padding: 10px 15px;
             position: relative;
             width: 100%;
         }
        .small, small {
            font-size: .875rem;
            font-weight: 400;
        }
        b, strong {
            font-weight: bolder;
        }
        .dashed-border {
            border: none;
            border-radius: 0;
        }
        .dashed-border {
            border: 3px dashed #e8e8e8;
            text-align: center;
        }
         .dashed-border {
             border: 3px dashed #e8e8e8;
             border-radius: 8px;
             text-align: center;
         }
        .newBrand .checkout-wrapper .checkout-items .disclaimers-link {
            color: #333;
        }
        .disclaimers-link {
            color: #999;
            cursor: pointer;
            float: left;
            font-size: 12px;
            margin: 0 0 0 15px;
            text-decoration: underline;
        }
        .empty-cart-button-wrapper {
            display: block;
            float: right;
            position: relative;
            left: 15px;
        }
        .empty-cart-button-wrapper .empty-cart-button {
            margin-right: 15px;
            min-height: 1rem;
            font-weight: bold;
            color: #000;
            position: relative;
            left: 15px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .prices-purchase {
            clear: both;
            padding-top: 25px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .price-summary {
            float: right;
            width: 100%;
        }
        .subtotal {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }
        .price {
            display: inline;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .charity, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .checkout-items h2, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .promo-add-link, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .promo-container, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .subtotal, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .taxes-fees {
            padding-left: 10px;
            padding-right: 10px;
        }
         .taxes-fees {
             display: flex;
             align-items: center;
             justify-content: space-between;
         }
        .taxes-fees .btn {
            padding: unset;
            color: #000;
            font-weight: bold;
        }
         .btn-link {
             color: #111!important;
         }
        .price {
            display: inline;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .price-summary .total-banner {
            margin-top: 5px;
        }
        .total-banner {
            border-top: 1px solid #ccc;
            border-bottom: none;
            padding: 25px 20px 10px;
            font-size: 32px;
            display: flex;
            align-items: center;
            justify-content: space-between;
            font-size: 1.5rem;
            padding: 10px 10px 5px;
            white-space: nowrap;
        }
        label {
            display: inline-block;
            margin-bottom: 4px;
            font-weight: 700;
        }
        .total-banner .formatted-money-currency {
            font-size: .8em;
        }
        .price {
            display: inline;
        }
        .text-primary-o {
            color: #09757a!important;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .price-summary {
            float: right;
            width: 100%;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .prices-purchase {
            clear: both;
            padding-top: 25px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .charity, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .checkout-items h2, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .promo-add-link, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .promo-container, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .subtotal, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .taxes-fees {
            padding-left: 10px;
            padding-right: 10px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .prices-purchase {
            clear: both;
            padding-top: 25px;
        }
        .checkout-items {
            background-color: #fff;
            padding: 20px 30px;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            float: left;
            width: 100%;
        }
        .btn .uxicon {
            margin-right: .35em;
            margin-left: .35em;
            vertical-align: -2px;
        }

        .uxicon {
            position: relative;
            display: inline-block;
            font-family: uxfont!important;
            font-style: normal;
            font-weight: 400;
            font-variant: normal;
            line-height: 1;
            text-decoration: none;
            text-transform: none;
            vertical-align: middle;
            speak: none;
        }
        .price-summary {
            float: right;
            width: 100%;
        }
        .sso-box {
            background-color: #fff;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            padding: 20px;
        }
        .sso-box .table-blocks {
            display: block;
        }
        .sso-box .table-blocks .sso-title {
            display: none;
            margin-left: 15px;
        }
        @media (min-width: 520px){
        .sso-box .table-blocks .sso-title {
            display: flex;
            }
        }
        .sso-box .table-blocks .sso-title .uxicon {
            display: none;
        }
         .sso-box .table-blocks .sso-title .uxicon {
             font-size: 1.5rem;
             margin: .5rem .5rem 0 0;
         }
         .sso-box h2 {
            font-size: 26px;
            font-weight: 400;
        }
        .sso-box .table-blocks .sso-flex-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap-reverse;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item {
            display: flex;
            flex: 1;
            flex-direction: column;
            padding: 0 15px 15px;
            min-width: 200px;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item .sso-question-header {
            font-weight: 700;
            font-size: 1rem;
            padding: 0;
            margin: 0;
        }
        @media (min-width: 992px){
            .h2, h2 {
                font-size: 2.02729rem;
                line-height: 40px;
            }
        }
        sso-box .table-blocks .sso-flex-container .sso-box-item .sso-text-body {
            flex-grow: 1;
            font-size: 1rem;
            padding-bottom: 10px;
        }
        .checkout-wrapper .checkout-view .c3-disclaimer button, .checkout-wrapper .checkout-view .purchase-btn button, .checkout-wrapper .checkout-view .sso-box button {
            width: 100%;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item {
            display: flex;
            flex: 1;
            flex-direction: column;
            padding: 0 15px 15px;
            min-width: 200px;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item .sso-question-header {
            font-weight: 700;
            font-size: 1rem;
            padding: 0;
            margin: 0;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item .sso-text-body {
            flex-grow: 1;
            font-size: 1rem;
            padding-bottom: 10px;
        }
        .checkout-wrapper .checkout-view .c3-disclaimer button, .checkout-wrapper .checkout-view .purchase-btn button, .checkout-wrapper .checkout-view .sso-box button {
            width: 100%;
        }
        .btn-primary {
            color: #fff!important;
            background: #111!important;
            border-color: #111!important;
        }
        .btn-default {
            color: #111!important;
            background: #fff!important;
            border-color: #111!important;
        }
        .with-top-border {
            border-top: 1px solid #edeff2;
            background: #e4e4e4;
        }
        .empty-basket-box {
            border-radius: 8px;
            background-color: #f5f7f8;
            color: #767676;
            padding: 30px 0 40px;
            text-align: center;
        }
        @media (min-width: 401px){
            .empty-basket-box .uxicon-cart {
                color: #fd7e14;
                display: inline-block;
                font-size: 110px;
                margin: 0 15px 25px 0;
            }
        }
         .empty-basket-box .uxicon-cart {
             display: inline-block;
         }

        @media (min-width: 992px) {

            .lead {
                font-size: 1.125rem;
            }

            .lead {
                font-size: 1.125rem;
                font-weight: 300;
            }
        }
        #domain-search-card {
            background: #e4e4e4 !important;
            border-color: #d6d6d6;
            border-style: solid;
            border-width: 1px 0 0;
            margin-top: 20px;
            padding: 20px;
        }
        #domain-search-card {
            background: inherit;
            border-color: #d6d6d6;
            border-style: solid;
            border-width: 1px 0 0;
            margin-top: 20px;
            padding: 20px;
        }
         #domain-search-card {
             border-radius: 0;
         }
         #domain-search-card {
             border: 3px dashed #e8e8e8;
             margin-top: 20px;
             padding: 20px;
         }
         .ux-card {
             border: 1px solid #d6d6d6;
         }
        #domain-search-card .search-title {
            display: block;
            margin-bottom: 15px;
        }
        #domain-search-card .dpp-search h3.headline-primary {
            display: none;
        }
         #domain-search-card .dpp-search h3.headline-primary {
             display: none;
         }
        @media (min-width: 768px) {

            .dpp-search .form-group, .dpp-search h3.label {
                margin-bottom: 25px;
            }

            .dpp-search .form-group, .dpp-search h3.label {
                margin-bottom: 15px;
            }
        }
        .dpp-search button {
            font-size: 20px;
            min-width: 50px;
            padding: 0px !important;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
            position: absolute;
            bottom: 0px;
            right: 0px;
            height: 38px;
        }
        #domain-search-header .inline-button-domain-order #search-btn {
            position: relative;
        }
        .alert-success {
            width: 100%;
            color: #fff !important;
            background-color: #101010 !important;
            border-color: #ffffff !important;
            text-align: center !important;
            font-size: 18px !important;
            font-weight: bold !important;
        }
        .checkout-payment {
            margin-bottom: 20px;
        }
        .checkout-payment .payment-gray-bkg {
            background-color: #fff;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            padding: 20px;
        }
         .checkout-payment .payment-gray-bkg {
             background-color: #f5f7f8;
             border-radius: 8px;
             padding: 20px;
         }
        .payment-radios .payment-methods-header {
            display: flex;
            align-items: center;
        }
        .checkout-payment .payment-gray-bkg h4 {
            font-size: 20px;
        }
         .payment-radios .payment-methods-header h4 {
             flex-grow: 1;
         }
        .checkout-payment .stored-payments .stored-payment {
            display: inline-block;
            width: 100%;
        }
        .payment-radios .ending-with-row {
            display: inline-block;
        }
        .payment-icon {
            display: inline-block;
            vertical-align: top;
            margin-right: 5px;
        }
        .payment-icon embed, .payment-icon img {
            font-size: 24px;
            height: auto;
            vertical-align: middle;
            width: 80px;
        }
        .payment-radios .ending-with-row .payment-display {
            display: inline-block;
        }
         .checkout-payment .ending-with-row .payment-display {
             font-weight: 400;
             color: #2b2b2b!important;
             font-size: .9em;
         }
         .payment-display {
             display: inline-flex;
             -webkit-flex-direction: column;
             flex-direction: column;
             -webkit-justify-content: flex-start;
             justify-content: flex-start;
             padding-left: 10px;
         }
        checkout-payment .ending-with-row .payment-display {
            font-weight: 400;
            color: #2b2b2b!important;
            font-size: .9em;
        }
        .checkout-payment .payment-gray-bkg {
            background-color: #fff;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            padding: 20px;
        }
         .customer-contact-box {
            background-color: #fff;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            padding: 20px;
            margin-bottom: 15px;
        }
        .customer-contact-box h2 {
            font-weight: 400;
            font-size: 26px;
        }
        .customer-contact-box .customer-contact {
            background-color: inherit;
            padding: 0;
            margin-bottom: 0;
        }
         .customer-contact-box .customer-contact {
             background-color: #fff;
             border-radius: 8px;
             padding: 15px;
             margin-bottom: 15px;
         }
        .customer-contact-box .customer-contact-form .update-billing-info {
            font-weight: 700;
            margin-bottom: 10px;
        }
        .customer-contact-box .customer-contact-form .form-group {
            margin-bottom: 10px;
        }
    </style>
    <div id="SDHost-header" class="d-flex mx-auto flex-column">
        <div class="bg_overlay_header">
            <img src="frontend/img/header/h_bg_01.svg" alt="img-bg">
        </div>

        @include('frontend.layouts.topmenu')

        <div class="mt-auto header-top-height"></div>
        <main class="container mb-auto">
            <div class="row">
                <div class="col-md-5 d-flex mx-auto flex-column">
                    <div class="mb-auto"></div>
                    <h3 class="mt-3 main-header-text-title">
                        <span>Stay in full control of your environment with our web hosting</span>Cart</h3>
                </div>
                <div class="col-md-7">
                    <div class="breadcrumb-hosting-pages row">
                        <a class="col-md-3" href="#">
                            <img src="frontend/img/svgs/hosting.svg" alt="" />
                            <span class="sub-breadcrumb-host-title">web hosting</span>
                        </a>
                        <a class="col-md-3 active" href="#">
                            <span class="off-tag">-15% off</span>
                            <img src="frontend/img/svgs/servers.svg" alt="" />
                            <span class="sub-breadcrumb-host-title">servers</span>
                        </a>
                        <a class="col-md-3" href="cloud-vps.html">
                            <img src="frontend/img/svgs/clouds.svg" alt="#" />
                            <span class="sub-breadcrumb-host-title">clouds VPS</span>
                        </a>
                        <a class="col-md-3" href="dedicated.html">
                            <img src="frontend/img/svgs/dedicated.svg" alt="" />
                            <span class="sub-breadcrumb-host-title">Dedicated</span>
                        </a>
                    </div>
                </div>
            </div>
        </main>
        <div class="mt-auto"></div>
    </div>

    <section class="padding-70-0 position-relative" >
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-12">
                    @if (session('success'))
                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>

                @if (Auth::check())
                    <?php
                    $id_user = Auth::user()->id;
                    $user_info = \App\User::where('users.id',$id_user)
                        ->join('apps_countries', 'users.billing_country', '=', 'apps_countries.country_code')
                        ->select('users.*', 'apps_countries.country_name')
                        ->get();
                    if(0 == count($user_info)){
                        $user_info_ = \App\User::where('users.id',$id_user)->get();
                    }else{
                        $user_info_ = \App\User::where('users.id',$id_user)
                            ->join('apps_countries', 'users.billing_country', '=', 'apps_countries.country_code')
                            ->select('users.*', 'apps_countries.country_name')
                            ->get();
                    }
                    ?>
                    @if (sizeof(Cart::content()) > 0)
{{--                        <div class="col-lg-6">--}}
{{--                            <div class="sso-box">--}}
{{--                                <div class="table-blocks">--}}
{{--                                    <div class="sso-title"><span class="uxicon uxicon-locked"></span>--}}
{{--                                        <h2> Continue shopping</h2></div>--}}
{{--                                    <div class="sso-flex-container">--}}


{{--                                        <div class="sso-box-item">--}}
{{--                                            <a href="{{'login'}}" class="btn btn-primary" id="" type="button">Checkout</a>--}}
{{--                                        </div>--}}
{{--                                        <div class="sso-box-item" style="min-width: 100% !important;">--}}
{{--                                            <div class="sso-text-body">Note: Domain name registration DNS global propagation takes between 30 minutes - 1h.</div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                <form action="{{url('PurchaseDomain')}}" method="post" style="width: 100%;display: inline-flex;">
                            {{ csrf_field() }}
                       <div class="col-lg-6">
                           <div class="checkout-box-col">
                               <div class="checkout-payment">
                                   <div class="payment-gray-bkg">
                                       <div class="stored-payments payment-radios">
                                           <div class="payment-methods-header">
                                               <h4>Payment</h4></div>
                                           <div class="stored-payment-area">
                                               <div class="stored-payment">
                                                   <div class="payment-option-disabled">
                                                       <div class="ending-with-row">
                                                           <div id="paymentIcon" class="payment-icon">
                                                               <img src="frontend/img/MTN_MM_logo_generic_2.png" alt="mtnmobilemoney">
                                                           </div>
                                                           <div class="payment-display"><span class="ctHidden">MTN Mobile Money</span></div>
                                                       </div>
                                                   </div>
                                                   <div class="name-fields">
                                                       <div class="ctHidden">
                                                           <fieldset class="form-group">
                                                               <label for="First-Name" id="label-First-Name">
                                                                   Phone Number <span class="req" aria-label="required">*</span>
                                                               </label>
                                                               <input id="First-Name" name="payment_number" placeholder="078384772" class="form-control"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                      type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10"required>
                                                           </fieldset>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                   </div>
                               </div>
                               <div class="customer-contact-box">
                                   <h2>Billing Information</h2>
                                   @foreach($user_info_ as $datas)
                                       <div class="customer-contact">
                                           <div class="update-billing-info">billing information </div>
                                           <div class="name-fields">
                                               <div class="ctHidden">
                                                   <fieldset class="form-group">
                                                       <label for="First-Name" id="label-First-Name">
                                                           Names <span class="req" aria-label="required">*</span>
                                                       </label>
                                                       <input id="First-Name" type="text" name="billing_names" class="ctHidden form-control" value="{{$datas->name}}" required>
                                                   </fieldset>
                                               </div>
                                           </div>
                                           <div class="name-fields">
                                               <div class="ctHidden">
                                                   <fieldset class="form-group">
                                                       <label for="First-Name" id="label-First-Name">
                                                           Email <span class="req" aria-label="required">*</span>
                                                       </label>
                                                       <input id="First-Name" type="email" name="billing_email" class="ctHidden form-control" value="{{$datas->email}}" required>
                                                   </fieldset>
                                               </div>
                                           </div>
                                           <fieldset class="form-group form-group-phone">
                                               <label>Primary Phone <span class="req">*</span></label>
                                               <div class="form-group">
                                                   <div class="ux-tel-container">
                                                       <fieldset class="form-group">
                                                           <input id="o"name="billing_phone" placeholder="25078384772" class="form-control ctHidden ux-tel-input" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                  type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="12" value="{{$datas->billing_phone}}" required>
                                                       </fieldset>
                                                   </div>
                                               </div>
                                           </fieldset>

                                           <div class="org-tax-fields">
                                               <fieldset class="form-group">
                                                   <label for="Organization" id="label-Organization">
                                                       Organization </label>
                                                   <input type="text" id="Organization" name="billing_organisation" value="{{$datas->billing_business_name}}" class="org-field form-control">
                                               </fieldset>
                                           </div>
                                           <div class="address-form undefined">
                                               <fieldset class="form-group has-danger">
                                                   <label for="address1" id="label-address1">
                                                       Address <span class="req" aria-label="required">*</span>
                                                   </label>
                                                   <input id="address1" name="billing_address" type="text" value="{{$datas->billing_address}}"  class="ctHidden form-control form-control-danger">
                                               </fieldset>

                                               <button tabindex="0" class="btn btn-link address2" id="" type="button">Add address line</button>

                                               <div class="postal-code-field">
                                                   <fieldset class="form-group">
                                                       <label for="postalCode" id="label-postalCode">
                                                           Postal code
                                                       </label>
                                                       <input id="postalCode" type="tel" name="billing_postal" value="{{$datas->billing_postal}}" class="ctHidden form-control">
                                                   </fieldset>
                                               </div>

                                               <div class="city-field">
                                                   <fieldset class="form-group has-danger">
                                                       <label for="city" id="label-city">
                                                           City
                                                           <span class="req" aria-label="required">*</span>
                                                       </label>
                                                       <input type="text" id="city" name="billing_city" class="ctHidden form-control form-control-danger" value="{{$datas->billing_city}}" required>
                                                   </fieldset>
                                               </div>
                                               <div class="city-field">
                                                   <fieldset class="form-group has-danger">
                                                       <label for="city" id="label-city">Country
                                                           <span class="req" aria-label="required">*</span>
                                                       </label>
                                                       <select class="form-control" name="billing_country" required>
                                                           <option value="{{$datas->billing_country}}">{{$datas->country_name}}</option>
                                                           @foreach($list as $country)
                                                               <option value="{{$country->country_code}}">{{$country->country_name}}</option>
                                                           @endforeach
                                                       </select>
                                                   </fieldset>
                                               </div>
                                           </div>
                                       </div>
                                   @endforeach

                               </div>
                               </div>
                           </div>
                        <div class="col-lg-6">
                            <div class="checkout-box-col">
                                <div class="checkout-items">
                                    <h2>Your Items</h2>
                                    <div>
                                        <div class="item-wrapper">
                                            <div class="product ux-card ux-card-bleed">
                                                @foreach (Cart::content() as $item)
                                                    <div class="product-info clearfix" data-eid="gce.cart.checkout.item.click">
                                                        <div class="product-name-desc"><span class="name">{{$item->name}}</span><span class="description-wrap description"> Domain Registration</span></div>
                                                        <div class="item-price-block"><span class="price text-primary-o">{{$item->price}} RWF</span>
                                                        </div>
                                                        <div class="duration">
                                                            <div class="stand-alone">
                                                                <div class="duration-option">
                                                                    <div class="duration-select">
                                                                        <select class="form-control">
                                                                            <option value="1 years" selected>1 years</option>
                                                                            <option value="2 years">2 years</option>
                                                                            <option value="3 years">3 years</option>
                                                                            <option value="4 years">4 years</option>
                                                                            <option value="5 years">5 years</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><span class="renews">Renews at {{$item->price}} RWF/yr</span>
                                                        <a href="{{ url('DeleteProduct',['id'=> $item->rowId])}}" class="btn btn-link remove">
                                                    <span class="uxicon uxicon-trash">
                                                        <i class="fas fa-trash"></i>
                                                    </span>
                                                        </a>
                                                    </div>
                                                @endforeach
                                                <div class="privacy-crosssell add-privacy-crossell">
                                                    <div class="add-privacy-container">
                                                        <div class="arrow-div"><small><strong>Just a reminder</strong> <br>Your information will be public <span id="privacy-tooltip"> <span role="button" aria-haspopup="true" class="uxicon uxicon-help" style="cursor: pointer; outline: none;"></span></span></small></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <span class="disclaimers-link" data-eid="gce.cart.checkout.disclaimers.click">View offer disclaimers</span>
                                            <div class="empty-cart-button-wrapper">
                                                <button data-eid="gce.cart.checkout.empty-cart.click" tabindex="0" class="btn btn-link btn-cart-action empty-cart-button" id="" type="button">
                                                <span class="uxicon uxicon-trash">
                                                    <i class="fas fa-trash"></i>
                                                </span>
                                                    Empty Cart
                                                </button>
                                            </div>
                                            <div class="prices-purchase">
                                                <div class="price-summary">
                                                    <div class="subtotal text-ninth">
                                                        <div>Subtotal</div>
                                                        <div class="price"><?php echo Cart::subtotal(); ?> RWF</div>
                                                    </div>
                                                    <div class="taxes-fees text-ninth">
                                                        <button data-eid="gce.cart.checkout.charges.click" tabindex="0" class="btn btn-link btn-sm text-default" id="" type="button">Taxes &amp; Fees</button>
                                                        <div class="price"><?php echo Cart::tax(); ?> RWF</div>
                                                    </div>
                                                    <div class="total-banner">
                                                        <label>Total <span class="formatted-money-currency">(RWF)</span></label>
                                                        <div class="price text-primary-o"><?php echo Cart::total(); ?> RWF</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="sso-flex-container" style="display: inline-block !important;order-top: 1px solid #ccc;margin-top: 10px;">
                                                <div class="sso-box-item" style="min-width: 100% !important;    margin-top: 15px;">
                                                    <div class="sso-text-body">Note: Domain name registration DNS global propagation takes between 30 minutes - 1h.</div>
                                                </div>
                                                <div class="sso-box-item" style="margin-top: 15px;">
                                                    <input type="hidden" name="domain_id" value="{{$item->id}}">
                                                    <input type="hidden" name="price" value="{{$item->price}}">
                                                    <input type="hidden" name="domain_name" value="{{$item->name}}">
                                                    <input type="hidden" name="quantity" value="1">
                                                    <button type="submit" class="btn btn-primary" style="width: 100%;">Checkout</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-container">
                                        <div id="cart-chat"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </form>
                    @else
                        <div class="col-lg-12">
                            <div class="checkout-wrapper">
                                <div class="ux-overlay-wrapper" style="min-height: 0px;">
                                    <div class="checkout-view container">
                                        <div class="checkout-box">
                                            <div class="checkout-box-col">
                                                <div class="checkout-items">
                                                    <div>
                                                        <div class="empty-basket-box lead">
                                                    <span class="uxicon uxicon-cart">
                                                         <i class="fas fa-shopping-cart"></i>
                                                    </span>
                                                            <br><span>There are no items in your basket.</span>
                                                            <br>
                                                            <a  href="{{'/'}}" tabindex="0" class="btn btn-link text-default" >Keep shopping</a>
                                                        </div>
                                                        <div>
                                                            <div id="domain-search-card" class="product ux-card ux-card-bleed"><span class="search-title">Add Domain</span>
                                                                <div class="dpp-search">
                                                                    <h3 class="headline-primary label">Find your Domain</h3>
                                                                    <form action="{{url('SearchDomain')}}" method="post" id="domain-search-header" class="col-md-12">
                                                                        {{ csrf_field() }}
                                                                        <i class="fas fa-globe"></i>
                                                                        <input type="text" placeholder="search for you domain now" id="domain" name="domains[]">
                                                                        <span class="inline-button-domain-order">
<button data-toggle="tooltip" data-placement="left" title="transfer" id="transfer-btn" type="submit" name="transfer" value="Transfer"><i class="fas fa-undo"></i></button>
<button data-toggle="tooltip" data-placement="left" title="search" id="search-btn" type="submit" name="submit" value="Search"><i class="fas fa-search"></i></button>
</span>
                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div></div>
                                                    </div>
                                                    <div class="chat-container">
                                                        <div id="cart-chat"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @else
                    @if (sizeof(Cart::content()) > 0)
                        <div class="col-lg-6">
                            <div class="sso-box">
                                <div class="table-blocks">
                                    <div class="sso-title"><span class="uxicon uxicon-locked"></span>
                                        <h2>Sign in to purchase</h2></div>
                                    <div class="sso-flex-container">
                                        <div class="sso-box-item">
                                            <div class="h2 sso-question-header">New to SDHost?</div>
                                            <div class="sso-text-body">Create an account now.</div>
                                            <a href="{{'register'}}" class="btn btn-default">Create Account</a>
                                        </div>
                                        <div class="sso-box-item">
                                            <div class="h2 sso-question-header">Have an account?</div>
                                            <div class="sso-text-body">Sign in now.</div>
                                            <a href="{{'login'}}" class="btn btn-primary" id="" type="button">Sign in</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="checkout-box-col">
                                <div class="checkout-items">
                                    <h2>Your Items</h2>
                                    <div>
                                        <div class="item-wrapper">
                                            <div class="product ux-card ux-card-bleed">
                                                @foreach (Cart::content() as $item)
                                                    <div class="product-info clearfix" data-eid="gce.cart.checkout.item.click">
                                                        <div class="product-name-desc"><span class="name">{{$item->name}}</span><span class="description-wrap description"> Domain Registration</span></div>
                                                        <div class="item-price-block"><span class="price text-primary-o">{{$item->price}} RWF</span>
                                                            {{--                                                    <div class="first-year-plus">1st year $11.99--}}
                                                            {{--                                                        <br>2+ years $17.99</div>--}}
                                                            {{--                                                    <div class="price text-primary-o"><span class="percent-off text-primary-o">16% off</span></div>--}}
                                                        </div>
                                                        <div class="duration">
                                                            <div class="stand-alone">
                                                                <div class="duration-option">
                                                                    <div class="duration-select">
                                                                        <select class="form-control">
                                                                            <option value="1 years" selected>1 years</option>
                                                                            <option value="2 years">2 years</option>
                                                                            <option value="3 years">3 years</option>
                                                                            <option value="4 years">4 years</option>
                                                                            <option value="5 years">5 years</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><span class="renews">Renews at {{$item->price}} RWF/yr</span>
                                                        <a href="{{ url('DeleteProduct',['id'=> $item->rowId])}}" class="btn btn-link remove">
                                                    <span class="uxicon uxicon-trash">
                                                        <i class="fas fa-trash"></i>
                                                    </span>
                                                        </a>
                                                    </div>
                                                @endforeach
                                                <div class="privacy-crosssell add-privacy-crossell">
                                                    <div class="add-privacy-container">
                                                        <div class="arrow-div"><small><strong>Just a reminder</strong> <br>Your information will be public <span id="privacy-tooltip"> <span role="button" aria-haspopup="true" class="uxicon uxicon-help" style="cursor: pointer; outline: none;"></span></span></small></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <span class="disclaimers-link" data-eid="gce.cart.checkout.disclaimers.click">View offer disclaimers</span>
                                            <div class="empty-cart-button-wrapper">
                                                <button data-eid="gce.cart.checkout.empty-cart.click" tabindex="0" class="btn btn-link btn-cart-action empty-cart-button" id="" type="button">
                                                <span class="uxicon uxicon-trash">
                                                    <i class="fas fa-trash"></i>
                                                </span>
                                                    Empty Cart
                                                </button>
                                            </div>
                                            <div class="prices-purchase">
                                                <div class="price-summary">
                                                    <div class="subtotal text-ninth">
                                                        <div>Subtotal</div>
                                                        <div class="price"><?php echo Cart::subtotal(); ?> RWF</div>
                                                    </div>
                                                    <div class="taxes-fees text-ninth">
                                                        <button data-eid="gce.cart.checkout.charges.click" tabindex="0" class="btn btn-link btn-sm text-default" id="" type="button">Taxes &amp; Fees</button>
                                                        <div class="price"><?php echo Cart::tax(); ?> RWF</div>
                                                    </div>
                                                    <div class="total-banner">
                                                        <label>Total <span class="formatted-money-currency">(RWF)</span></label>
                                                        <div class="price text-primary-o"><?php echo Cart::total(); ?> RWF</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-container">
                                        <div id="cart-chat"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-12">
                            <div class="checkout-wrapper">
                                <div class="ux-overlay-wrapper" style="min-height: 0px;">
                                    <div class="checkout-view container">
                                        <div class="checkout-box">
                                            <div class="checkout-box-col">
                                                <div class="checkout-items">
                                                    <div>
                                                        <div class="empty-basket-box lead">
                                                    <span class="uxicon uxicon-cart">
                                                         <i class="fas fa-shopping-cart"></i>
                                                    </span>
                                                            <br><span>There are no items in your basket.</span>
                                                            <br>
                                                            <a  href="{{'/'}}" tabindex="0" class="btn btn-link text-default" >Keep shopping</a>
                                                        </div>
                                                        <div>
                                                            <div id="domain-search-card" class="product ux-card ux-card-bleed"><span class="search-title">Add Domain</span>
                                                                <div class="dpp-search">
                                                                    <h3 class="headline-primary label">Find your Domain</h3>
                                                                    <form action="{{url('SearchDomain')}}" method="post" id="domain-search-header" class="col-md-12">
                                                                        {{ csrf_field() }}
                                                                        <i class="fas fa-globe"></i>
                                                                        <input type="text" placeholder="search for you domain now" id="domain" name="domains[]">
                                                                        <span class="inline-button-domain-order">
<button data-toggle="tooltip" data-placement="left" title="transfer" id="transfer-btn" type="submit" name="transfer" value="Transfer"><i class="fas fa-undo"></i></button>
<button data-toggle="tooltip" data-placement="left" title="search" id="search-btn" type="submit" name="submit" value="Search"><i class="fas fa-search"></i></button>
</span>
                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div></div>
                                                    </div>
                                                    <div class="chat-container">
                                                        <div id="cart-chat"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif

            </div>
        </div>
    </section>
    <section class="padding-100-0 with-top-border">
        <div class="container">
            <h5 class="title-default-SDHost-two">Frequently asked questions.</h5>
            <div class="row justify-content-center mr-tp-40">
                <div class="col-md-9">
                    <div class="accordion" id="frequently-questions">
                        <div class="questions-box">
                            <div id="headingOne">
                                <button class="btn questions-title" type="button" data-toggle="collapse" data-target="#questionone" aria-expanded="true" aria-controls="questionone">
                                    How Can I Order New Host ?
                                </button>
                            </div>
                            <div id="questionone" class="collapse show questions-reponse" aria-labelledby="headingOne" data-parent="#frequently-questions">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                        <div class="questions-box">
                            <div id="headingtwo">
                                <button class="btn questions-title collapsed" type="button" data-toggle="collapse" data-target="#questiontwo" aria-expanded="true" aria-controls="questiontwo">
                                    What Is Resellers Hosting ?
                                </button>
                            </div>
                            <div id="questiontwo" class="collapse questions-reponse" aria-labelledby="headingtwo" data-parent="#frequently-questions">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                        <div class="questions-box">
                            <div id="headingtree">
                                <button class="btn questions-title collapsed" type="button" data-toggle="collapse" data-target="#questiontree" aria-expanded="true" aria-controls="questiontree">
                                    I Want New Domain Name
                                </button>
                            </div>
                            <div id="questiontree" class="collapse questions-reponse" aria-labelledby="headingtree" data-parent="#frequently-questions">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@include('frontend.layouts.footer')
@endsection