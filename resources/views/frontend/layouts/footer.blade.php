
<section class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-md-9 quiq-links-footer">
                <h5 class="quiq-links-footer-title">Quick Links</h5>
                <div class="row">
                    <ul class="col-md-6 quiq-links-footer-ul">
                        <li><a href="#">our company announcements</a></li>
                        <li><a href="#">Knowledgebase</a></li>
                        <li><a href="#">Downloads</a></li>
                        <li><a href="#">Network Status</a></li>
                        <li><a href="#">My Support Tickets</a></li>
                        <li><a href="#">Register a New Domain</a></li>
                        <li><a href="#">Transfer New Domain</a></li>
                        <li><a href="#">Software Products</a></li>
                        <li><a href="#">Dedicated Hosting</a></li>
                    </ul>
                    <ul class="col-md-6 quiq-links-footer-ul">
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Network Status</a></li>
                        <li><a href="#">Forgot Password?</a></li>
                        <li><a href="#">Create an account with us</a></li>
                        <li><a href="#">Login to your account</a></li>
                        <li><a href="#">make a new payment</a></li>
                        <li><a href="#">Review & Checkout</a></li>
                        <li><a href="#">client area</a></li>
                        <li><a href="#">manage your account</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <h5 class="quiq-links-footer-title">secure and contact</h5>
                <p class="secure-img-footer-area">
                    <img src="img/footer/secure.png" alt="" />
                    <span>this is for demo reason only</span>
                </p>
                <div class="footer-contact-method">
                    <a href="#">
                        <span>email us :</span>
                        <b><span class="__cf_email__" data-cfemail="">info@sdhost.rw</span></b>
                        <i class="fas fa-at"></i>
                    </a>
                    <a href="#">
                        <span>call us :</span>
                        <b>+250 782 384 772</b>
                        <i class="fas fa-phone"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="mr-tp-40 row justify-content-between footer-area-under">
            <div class="col-md-4">
                <a href="#"><img class="footer-logo-blue" src="img/header/logo-w-f.png" alt="" /></a>
                <div class="footer-social-icons">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                </div>
            </div>
            <div class="col-md-4 row">
                <ul class="col-md-6 under-footer-ullist">
                    <li><a href="#">about us</a></li>
                    <li><a href="#">our services</a></li>
                </ul>
                <ul class="col-md-6 under-footer-ullist text-right">
                    <li><a href="#">privacy policy</a></li>
                    <li><a href="#">terms of sevice</a></li>
                </ul>
            </div>
        </div>
        <div class="row justify-content-between final-footer-area mr-tp-40">
            <div class="final-footer-area-text">
                © Copyright 2020 SDHost , made with love in RWANDA
            </div>
        </div>
    </div>
</section>

<script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src="frontend/js/jquery.min.js"></script>
<script src="frontend/js/popper.min.js"></script>

<script src="frontend/js/bootstrap.min.js"></script>

<script src="frontend/js/template-scripts.js"></script>

<script src="frontend/js/flickity.pkgd.min.js"></script>

<script src="frontend/owlcarousel/owl.carousel.min.js"></script>

<script src="frontend/js/parallax.min.js"></script>

<script src="frontend/js/mailchamp.js"></script>

<script src="frontend/js/bootstrap.offcanvas.min.js"></script>

<script src="js/jquery.touchSwipe.min.js"></script>

<script src="js/demo.js"></script>