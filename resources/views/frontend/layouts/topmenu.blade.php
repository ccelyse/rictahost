<nav id="SDHost-navbar-header" class="navbar navbar-expand-md fixed-header-layout">
    <div class="container main-header-SDHost-s">
        <a class="navbar-brand" href="{{'/'}}"><img src="frontend/img/sdbleu.png" alt="" /></a>
        <button class="navbar-toggle offcanvas-toggle menu-btn-span-bar ml-auto" data-toggle="offcanvas" data-target="#offcanvas-menu-home">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="collapse navbar-collapse navbar-offcanvas" id="offcanvas-menu-home">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="{{'/'}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{'Domains'}}">Domains</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{'/'}}">Hosting</a>
                </li>


                @if (Auth::check())
                    @if(Auth::user()->role == "admin")
                    <li class="nav-item">
                        <a class="nav-link" id="myaccount" href="{{'Admin/AdminDashboard'}}"> <?php
                            $name = Auth::user()->name;
                            echo $name;
                            ?> (My Account) </a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" id="myaccount" href="{{'Client/Dashboard'}}"> <?php
                            $name = Auth::user()->name;
                            echo $name;
                            ?> (My Account) </a>
                    </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{'login'}}">Sign In</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{'register'}}">Create Account</a>
                    </li>
                @endif

            </ul>
        </div>
        <ul class="header-user-info-SDHost">
            <li class="dropdown">
                <a  id="header-login-dropdown" href="{{'CartDomain'}}">
                    Cart ( {{ Cart::instance('default')->count(false) }} )
                </a>
            </li>
        </ul>
    </div>
</nav>