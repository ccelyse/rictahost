@extends('frontend.layouts.master')

@section('title', 'SD Host')

@section('content')
    <style>
        .ds-exact-match-wrap, .ds-spin-and-filter-wrap, .ds-tld-gallery-wrap {
            background-color: #f5f7f8 !important;
            border-radius: 0;
            padding: 0.5rem;
        }
        .exact-match-wrap {
            background: #fff;
            padding: 16px 0;
        }
        .bg-medium {
            color: #111;
            background-color: #f5f7f8;
        }
        card {
            padding: 0;
            margin: 0 !important;
            background: #fff;
            position: relative;
        }
        .ds-exact-block-card {
            border-radius: 0;
        }
        .ux-card {
            border-radius: 4px;
            margin-top: 20px;
            color: #111;
            /*border: 1px solid #d4dbe0;*/

        }
        .ds-flatten {
            box-shadow: none !important;
            border-radius: 0 !important;
        }
        .dpp-results .ds-exact-header {
            border-radius: 0;
        }
        .ds-available.ds-intl {
            background-color: #09757a;
        }
        .dpp-results .exact-header {
            height: 30px;
            padding: 0 16px;
            color: #FFF;
            display: flex;
            justify-content: space-between;
            align-items: center;
            position: relative;
            border-radius: 8px 8px 0 0;
        }
        .dpp-results .exact-header-tag {
            font-size: 16px;
            font-weight: bold;
        }
        .ds-exact-header-tag {
            font-size: .875rem !important;
            margin-left: 1.5rem;
        }
        .dpp-results .exact-header-call {
            font-size: 12px;
            line-height: 14px;
        }
        .ds-exact-header-call {
            margin-right: 1.5rem !important;
        }
        .dpp-results .ds-exact-body {
            padding: 1rem 2.25rem;
        }
        .dpp-results .exact-body {
            min-height: 80px;
            padding: 16px;
            display: flex;
            justify-content: space-between;
            position: relative;
            background: #FFF;
            z-index: 1;
        }
        .dpp-results .exact-body-result {
            flex-grow: 1;
            width: calc(100% - 346px);
        }
        .dpp-results .ds-exact-body .domain-name {
            padding-bottom: 1.5rem;
        }
        ds-exact-body.ds-intl .domain-name {
            padding-bottom: 0 !important;
        }
        .dpp-results .exact-body-result .domain-name {
            display: block;
            position: relative;
            min-height: 24px;
            width: 100%;
            margin-bottom: 6px;
        }
        .dpp-results .ds-exact-body .ds-domain-name-text {
            font-size: 1.625rem;
            line-height: 1.625rem;
            font-weight: normal;
        }
        .dpp-results .exact-body-result .domain-name-text {
            width: 100%;
            display: block;
            left: 0;
            transform-origin: top left;
            font-size: 24px;
            font-weight: 700;
            line-height: 24px;
            margin-bottom: 0;
            white-space: normal;
            overflow-wrap: break-word;
            word-wrap: break-word;
            font-family:Circular !important;
        }
        .dpp-results .exact-body-result .price-with-domain .h2 {
            display: none;
        }
        .dpp-results .exact-body-result .price-with-domain .price-block {
            font-size: 14px;
        }
        .dpp-results .exact-body-result .price-with-domain .price-block span {
            display: inline-block;
        }
        .d-block {
            display: block!important;
        }
        .d-inline-block {
            display: inline-block!important;
        }
        .dpp-results .exact-body-result .price-with-domain .price-block span {
            display: inline-block;
        }
         .dpp-results .ds-exact-body .price-with-domain .ds-terms {
             line-height: 2.5rem !important;
         }
         .ds-exact-body.ds-intl .price-with-domain .ds-terms {
             line-height: 1rem !important;
         }
         .price-with-domain .ds-terms {
             color: #111;
             font-size: .875rem;
         }
         .terms, .renewal-price {
             position: relative;
             top: -.2rem;
         }
         .ds-terms {
             color: #757575;
             font-size: .75rem;
             line-height: .9375rem;
         }
        .ms-2 {
            font-size: .79012rem;
        }
        .ds-exact-body.ds-intl .cross-sell-container {
            margin-top: .5rem !important;
        }
        .dpp-results .cross-sell-container .cross-sell-wrap {
            background: #fff;
            position: relative;
            clear: both;
            width: 100%;
            border: 0;
            padding: 0;
            clear: both;
            color: #333;
            font-size: 14px;
            min-width: 0;
            background: none;
        }
        .dpp-results .exact-body-result .cross-sell-wrap {
            width: auto !important;
            position: absolute;
            left: 0;
            transform-origin: top left;
            background: transparent;
        }
        .dpp-results .domain-result, .dpp-results .bundle-domain-result, .dpp-results .cross-sell-container .cross-sell-wrap {
            border: 1px solid #E8E8E8;
            float: left;
            min-width: 290px;
            padding: 10px 20px;
            vertical-align: middle;
            width: 99%;
        }
        .font-base {
            font-family: gdsherpa,Helvetica,Arial,sans-serif;
        }
        .dpp-results .cross-sell-container .cross-sell-wrap .form-group {
            margin-bottom: 0;
        }
        .form-group {
            margin-bottom: 24px;
            position: relative;
        }
        fieldset {
            min-width: 0;
            padding: 0;
            margin: 0;
            border: 0;
        }
        .dpp-results .cross-sell-container .cross-sell-wrap .form-group .form-check {
            margin-bottom: 0;
        }

        .form-check {
            position: relative;
            display: flex;
            margin-bottom: .5rem;
        }
        .custom-control {
            font-family: gdsherpa,Helvetica,Arial,sans-serif;
            position: relative;
            display: inline-flex;
            flex-direction: column;
            min-height: 1.5rem;
            padding-left: 1.8rem;
            margin-right: 1rem;
            transition: .3s all ease-in-out;
        }
        .form-check-label {
            margin-bottom: 0;
        }
        .form-check-inline, .form-check-label {
            font-family: gdsherpa,Helvetica,Arial,sans-serif;
            padding-left: 1.3rem;
            font-size: 1em;
        }
        label {
            display: inline-block;
            margin-bottom: 4px;
            font-weight: 700;
        }
        .dpp-results .cross-sell-container .cross-sell-wrap .cross-sell-renew {
            display: block;
            font-size: 12px;
            padding-left: 30px;
        }

        .text-muted {
            color: #767676!important;
        }
        .dpp-results .exact-body-help {
            width: 330px;
            margin-left: 16px;
        }
        .dpp-results .exact-body-help-title {
            font-size: 18px;
            line-height: 18px;
            margin: 0 0 6px !important;
            white-space: nowrap;
        }
         .ds-exact-body-help-title {
             font-size: 1.4375rem !important;
             line-height: 1.625rem !important;
         }
        .dpp-results .exact-body-help-line {
            margin-bottom: 0 !important;
        }
         .ds-help-line {
             font-size: 1rem;
             line-height: 1.5rem;
         }
        .ds-help-line {
            font-size: 1rem;
            line-height: 1.5rem;
        }
        .dpp-results .exact-body-help-line {
            margin-bottom: 0 !important;
        }
        .dpp-results .exact-body-help-line {
            margin-bottom: 0 !important;
        }
         .ds-help-line {
             font-size: 1rem;
             line-height: 1.5rem;
         }
        .dpp-results .exact-footer {
            padding: 0 16px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            background-color: #F3F3F3;
            position: relative;
            border-radius: 0 0 8px 8px;
            box-shadow: 0 3px 6px rgba(117,117,117,0.25);
            z-index: 1;
        }
         .ds-flatten {
             box-shadow: none !important;
             border-radius: 0 !important;
         }
         .ds-exact-footer {
             border-radius: 0 !important;
             height: 4.375rem !important;
             padding: 1rem 2.25rem !important;
         }
        .bg-white {
            background-color: #fff!important;
        }
        .bg-white, .bg-white-base {
            background-color: #fff;
            color: #111;
        }
        .dpp-results .exact-footer-links {
            display: flex;
            flex-flow: row wrap;
            justify-content: center;
        }
        .dpp-results .exact-footer-video {
            margin-right: 15px;
        }
        .dpp-results .ds-exact-footer-video.ds-intl .ds-video-link {
            color: #09757a !important;
        }
         .dpp-results .ds-exact-footer-video .ds-video-link {
             color: #111 !important;
             line-height: 1.125rem;
             cursor: pointer;
         }
         .dpp-results .exact-footer-video a, .dpp-results .exact-footer-video a:hover {
             color: #1976d2 !important;
             vertical-align: middle;
             font-size: 14px;
             font-weight: normal;
             cursor: pointer;
         }
        .dpp-results .exact-footer-buttons {
            height: 100%;
            align-items: center;
            background-color: #F3F3F3;
        }
         .dpp-results .exact-footer-buttons {
             display: flex;
         }
        .text-right {
            text-align: right!important;
            justify-content: right!important;
        }
        .ds-exact-footer.ds-intl .exact-footer-buttons .btn.btn-primary {
            background-color: #09757a !important;
            border-color:transparent !important;
            border-radius: 0px !important;
        }
         .ds-exact-footer .exact-footer-buttons .btn.btn-primary, .ds-exact-footer .exact-footer-buttons .btn.btn-purchase {
             background-color: #145fa9 !important;
             color: #fff;
         }
         .dpp-results .ds-exact-footer .btn-primary {
             min-width: 6.25rem;
         }
         .dpp-results .exact-footer-buttons button {
             margin-left: 10px;
             text-decoration: none;
         }
        .exact-match-wrap .new-exact-block-card.gd.taken .exact-header {
            background-color: #000 !important;
        }
        .ds-taken{
            background: #000;
        }
        .alert-success {
            color: #155724 !important;;
            background-color: #ffffff !important;
            border-color: #ffffff !important;
            text-align: center !important;;
            font-size: 25px !important;;
            font-weight: bold !important;;
        }
    </style>
    <div id="SDHost-header" class="d-flex mx-auto flex-column">
        <div class="bg_overlay_header">
            <img src="frontend/img/header/h_bg_01.svg" alt="img-bg">
        </div>

        @include('frontend.layouts.topmenu')

        <div class="mt-auto header-top-height"></div>

        <main class="container mb-auto">
            <div class="carousel carousel-main">
                <div class="carousel-cell">
                    <h3 class="mt-3 main-header-text-title"><span>the best domain provider in the area</span>secure and guaranteed <small>order you own now</small></h3>
                    <form action="{{url('SearchDomain')}}" method="post" id="domain-search-header" class="col-md-6">
                        {{ csrf_field() }}
                        <i class="fas fa-globe"></i>
                        <input type="text" placeholder="search for you domain now" id="domain" name="domains[]">
                        <span class="inline-button-domain-order">
<button data-toggle="tooltip" data-placement="left" title="transfer" id="transfer-btn" type="submit" name="transfer" value="Transfer"><i class="fas fa-undo"></i></button>
<button data-toggle="tooltip" data-placement="left" title="search" id="search-btn" type="submit" name="submit" value="Search"><i class="fas fa-search"></i></button>
</span>
                    </form>
                    @if (session('success'))
                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                            {{ session('success') }}
                        </div>
                    @endif

                </div>
                </div>
            <nav class="nav-header-chage nav--shamso carousel-nav">
                <button class="nav__item nav__item--current carousel-cell" aria-label="Item 1"><span class="nav__item-title">Domains</span></button>
                <button class="nav__item carousel-cell" aria-label="Item 2"><span class="nav__item-title">Hosting</span></button>
            </nav>
        </main>
        <div class="mt-auto"></div>
    </div>


    <section class="padding-70-0 position-relative">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-8 about-us-img-section">
                    <img src="frontend/img/demo/groupofworks.jpg" alt="" />
                </div>
                <div class="col-md-4 side-text-right-container d-flex mx-auto flex-column">
                    <div class="mb-auto"></div>
                    <h2 class="side-text-right-title f-size25">We are with you ,<br> every step of the way</h2>
                    <p class="side-text-right-text f-size16">
                        Whether you are looking for a <b>personal</b> website hosting plan or a <b>business</b> website hosting plan, We are the perfect solution for you. Our powerful website hosting services will not only help you achieve your overall website goals, but will also provide you with the confidence you need in knowing that you are partnered with a <a href="#">reliable</a> and <a href="#">secure</a> website hosting platform.
                        <br>
                    </p>
                    <p>
                        <a class="side-text-right-button" href="#">start with us now</a>
                    </p>
                    <div class="mt-auto"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="padding-100-0 with-top-border">
        <div class="container">
            <h5 class="title-default-SDHost-two">Frequently asked questions.</h5>
            <div class="row justify-content-center mr-tp-40">
                <div class="col-md-9">
                    <div class="accordion" id="frequently-questions">
                        <div class="questions-box">
                            <div id="headingOne">
                                <button class="btn questions-title" type="button" data-toggle="collapse" data-target="#questionone" aria-expanded="true" aria-controls="questionone">
                                    How Can I Order New Host ?
                                </button>
                            </div>
                            <div id="questionone" class="collapse show questions-reponse" aria-labelledby="headingOne" data-parent="#frequently-questions">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                        <div class="questions-box">
                            <div id="headingtwo">
                                <button class="btn questions-title collapsed" type="button" data-toggle="collapse" data-target="#questiontwo" aria-expanded="true" aria-controls="questiontwo">
                                    What Is Resellers Hosting ?
                                </button>
                            </div>
                            <div id="questiontwo" class="collapse questions-reponse" aria-labelledby="headingtwo" data-parent="#frequently-questions">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                        <div class="questions-box">
                            <div id="headingtree">
                                <button class="btn questions-title collapsed" type="button" data-toggle="collapse" data-target="#questiontree" aria-expanded="true" aria-controls="questiontree">
                                    I Want New Domain Name
                                </button>
                            </div>
                            <div id="questiontree" class="collapse questions-reponse" aria-labelledby="headingtree" data-parent="#frequently-questions">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@include('frontend.layouts.footer')
@endsection