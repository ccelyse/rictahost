@extends('frontend.layouts.master')

@section('title', 'SD Host')

@section('content')
    <div id="SDHost-header" class="d-flex mx-auto flex-column">
        <div class="bg_overlay_header">
            <img src="frontend/img/header/h_bg_01.svg" alt="img-bg">
        </div>

        @include('frontend.layouts.topmenu')

        <div class="mt-auto header-top-height"></div>

        <main class="container mb-auto">
            <div class="carousel carousel-main">
                <div class="carousel-cell">
                    <h3 class="mt-3 main-header-text-title"><span>the best domain provider in the area</span>secure and guaranteed <small>order you own now</small></h3>

                    <form action="{{url('SearchDomain')}}" method="post" id="domain-search-header" class="col-md-6">
                        {{ csrf_field() }}
                        <i class="fas fa-globe"></i>
                        <input type="text" placeholder="search for you domain now" id="domain" name="domains[]" required>
                        <span class="inline-button-domain-order">
<button data-toggle="tooltip" data-placement="left" title="transfer" id="transfer-btn" type="submit" name="transfer" value="Transfer"><i class="fas fa-undo"></i></button>
<button data-toggle="tooltip" data-placement="left" title="search" id="search-btn" type="submit" name="submit" value="Search"><i class="fas fa-search"></i></button>
</span>
                    </form>

                    <span class="col-md-6 domain-search-header-pricetext">starting at <b>13,000 RWF/year</b></span>
                    <div class="owl-theme owl-domain-prices-previw col-md-7">
                        <div class="domain-name-classes item">
                            <div class="domain-img"><img src="frontend/img/domain/black/rw.png" alt="" /></div>
                            <span class="price">13,000 RWF/year</span>
                            <span class="features-domains">
<a data-toggle="tooltip" data-placement="right" title="on sale"><img src="frontend/img/svgs/Time.svg" alt="" /></a>
<a data-toggle="tooltip" data-placement="right" title="secure"><img src="frontend/img/svgs/Locked.svg" alt="" /></a>
</span>
                        </div>
                        <div class="domain-name-classes item">
                            <div class="domain-img"><img src="frontend/img/domain/black/corw.png" alt="" /></div>
                            <span class="price">13,000 RWF/year</span>
                            <span class="features-domains">
<a data-toggle="tooltip" data-placement="right" title="on sale"><img src="frontend/img/svgs/Time.svg" alt="" /></a>
<a data-toggle="tooltip" data-placement="right" title="secure"><img src="frontend/img/svgs/Locked.svg" alt="" /></a>
</span>
                        </div>
                        <div class="domain-name-classes item">
                            <div class="domain-img"><img src="frontend/img/domain/black/orgrw.png" alt="" /></div>
                            <span class="price">13,000 RWF/year</span>
                            <span class="features-domains">
<a data-toggle="tooltip" data-placement="right" title="on sale"><img src="frontend/img/svgs/Time.svg" alt="" /></a>
<a data-toggle="tooltip" data-placement="right" title="secure"><img src="frontend/img/svgs/Locked.svg" alt="" /></a>
</span>
                        </div>
                        <div class="domain-name-classes item">
                            <div class="domain-img"><img src="frontend/img/domain/black/netrw.png" alt="" /></div>
                            <span class="price">13,000 RWF/year</span>
                            <span class="features-domains">
<a data-toggle="tooltip" data-placement="right" title="on sale"><img src="frontend/img/svgs/Time.svg" alt="" /></a>
<a data-toggle="tooltip" data-placement="right" title="secure"><img src="frontend/img/svgs/Locked.svg" alt="" /></a>
</span>
                        </div>
                        <div class="domain-name-classes item">
                            <div class="domain-img"><img src="frontend/img/domain/black/acrw.png" alt="" /></div>
                            <span class="price">13,000 RWF/year</span>
                            <span class="features-domains">
<a data-toggle="tooltip" data-placement="right" title="on sale"><img src="frontend/img/svgs/Time.svg" alt="" /></a>
<a data-toggle="tooltip" data-placement="right" title="secure"><img src="frontend/img/svgs/Locked.svg" alt="" /></a>
</span>
                        </div>
                        <div class="domain-name-classes item">
                            <div class="domain-img"><img src="frontend/img/domain/black/cooprw.png" alt="" /></div>
                            <span class="price">13,000 RWF/year</span>
                            <span class="features-domains">
<a data-toggle="tooltip" data-placement="right" title="on sale"><img src="frontend/img/svgs/Time.svg" alt="" /></a>
<a data-toggle="tooltip" data-placement="right" title="secure"><img src="frontend/img/svgs/Locked.svg" alt="" /></a>
</span>
                        </div>
                    </div>
                </div>
{{--                <div class="carousel-cell">--}}
{{--                    <div class="row hosting-header-slider-cell">--}}
{{--                        <div class="col-md-6">--}}
{{--                            <h3 class="mt-3 main-header-text-title">--}}
{{--                                <i class="circle-sub-title-header-slider">the best web hosting provider</i>--}}
{{--                                Perfect Plans For Getting <br>Started--}}
{{--                                <small>order yours now</small>--}}
{{--                            </h3>--}}
{{--                            <p class="text-sub-title-header-slider">Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>--}}
{{--                            <a class="btn-sub-title-header-slider" href="#">start now</a>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-6 text-center">--}}
{{--                            <img class="hosting-header-slider-cell-img" src="frontend/img/header/slider/header-bg.png" alt="" />--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <nav class="nav-header-chage nav--shamso carousel-nav">
                <button class="nav__item nav__item--current carousel-cell" aria-label="Item 1"><span class="nav__item-title">Domains</span></button>
                <button class="nav__item carousel-cell" aria-label="Item 2"><span class="nav__item-title">Hosting</span></button>
            </nav>
        </main>
        <div class="mt-auto"></div>
    </div>
<section class="padding-100-0-50">
    <div class="container">
        <h5 class="title-default-SDHost-two">Go where the pros host.<br>
            Web hosting that scales from easy to expert</h5>
        <div class="row justify-content-start">
            <div class="col-md-4">
                <div class="icon-hom-page-service-holder"><img src="frontend/img/svgs/Detailed/Battery.svg" alt="" /></div>
                <h5 class="icon-hom-page-service-title">web hosting</h5>
                <p class="icon-hom-page-service-text">Need more power? Dedicated Control Full Access and Scalable Resources.</p>
            </div>
            <div class="col-md-4">
                <div class="icon-hom-page-service-holder"><img src="frontend/img/svgs/Detailed/Download.svg" alt="" /></div>
                <h5 class="icon-hom-page-service-title">WordPress Hosting</h5>
                <p class="icon-hom-page-service-text">Built for Speed Advanced Security Free Migrations.</p>
            </div>
            <div class="col-md-4">
                <div class="icon-hom-page-service-holder"><img src="frontend/img/svgs/Detailed/VR.svg" alt="" /></div>
                <h5 class="icon-hom-page-service-title">domains</h5>
                <p class="icon-hom-page-service-text">Find your perfect domain name with us.</p>
            </div>
            <div class="col-md-4">
                <div class="icon-hom-page-service-holder"><img src="frontend/img/svgs/Detailed/Charger.svg" alt="" /></div>
                <h5 class="icon-hom-page-service-title">SSl Certificate</h5>
                <p class="icon-hom-page-service-text">Show visitors you’re trustworthy and authentic. </p>
            </div>
            <div class="col-md-8 mr-tp-60 hom-page-service-more">
                <span class="hom-page-service-more-banner"><img src="frontend/img/bg/cercle.png" alt="" /></span>
                <h5 class="hom-page-service-more-title">check out our latest offers <span></span></h5>
                <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate hom-page-service-more-form" target="_blank">
                    <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>
                        <div style="position: absolute; left: -5000px;" aria-hidden="true">
                            <input type="text" name="b_49eb75689a0b6e7eb66b67e51_4366ef611f" tabindex="-1" value="">
                        </div>
                        <div class="clear">
                            <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="padding-100-0 position-relative">
    <div class="body_overlay_ono"></div>
    <div class="container">
        <h5 class="title-default-SDHost-two">check out awesome plans, and order now <span class="mr-tp-20">chose which package is best for you.</span></h5>
        <div class="row justify-content-center">
            <div id="monthly-yearly-chenge" class="mr-tp-40 style-two">
                <a class="active monthly-price"> <span class="change-box-text">billed monthly</span> <span class="change-box"></span></a>
                <a class="yearli-price"> <span class="change-box-text">billed annually</span></a>
            </div>
        </div>
        <div class="row justify-content-start second-pricing-table-container mr-tp-30">
            <div class="col-md-4">
                <div class="second-pricing-table">
                    <h5 class="second-pricing-table-title">Starter Plan<span>When building a website, start here. Our shared service delivers a powerful, proven platform that's perfect for hosting your websites.</span></h5>
                    <span class="second-pricing-table-price monthly">
<i class="monthly">6000 RWF<small>/mo</small></i>
<i class="yearly">50,000 RWF<small>/year</small></i>
</span>
                    <ul class="second-pricing-table-body">
                        <li>1GB Storage Space</li>
                        <li>10GB Bandwidth</li>
                        <li>10 Free Sub-Domains</li>
                        <li>10 Database</li>
                        <li>Accounts Control Panel & FTP</li>
                        <li class="not-chacked">SSL Certificate</li>
                    </ul>
                    <a class="second-pricing-table-button" href="#">next setup</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="second-pricing-table">
                    <h5 class="second-pricing-table-title">Expert plan <span>Hosting is what makes your site visible on the web. We offer fast, reliable plans for you</span></h5>
                    <span class="second-pricing-table-price monthly">
<i class="monthly">8000 RWF<small>/mo</small></i>
<i class="yearly">75,000 RWF<small>/year</small></i>
</span>
                    <ul class="second-pricing-table-body">
                        <li>2GB Storage Space</li>
                        <li>20GB Bandwidth</li>
                        <li>30 Free Sub-Domains</li>
                        <li>30 Database</li>
                        <li>Free SSL Certificate</li>
                        <li>Accounts Control Panel & FTP</li>
                    </ul>
                    <a class="second-pricing-table-button" href="#">next setup</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="second-pricing-table active">
                    <h5 class="second-pricing-table-title">Enterprise plan <span>Our new Enterprise Hosting solution is powerful,
affordable, and easier than ever.</span></h5>
                    <span class="second-pricing-table-price monthly">
<i class="monthly">12000 RWF<small>/mo</small></i>
<i class="yearly">120,000 RWF<small>/year</small></i>
</span>
                    <ul class="second-pricing-table-body">
                        <li>Unmetered Storage Space</li>
                        <li>Unlimited Free Sub-Domains</li>
                        <li>Unlimited Database</li>
                        <li>Unlimited E-mail</li>
                        <li>Free SSL Certificate</li>
                        <li>Accounts Control Panel & FTP</li>
                    </ul>
                    <a class="second-pricing-table-button" href="#">next setup</a>
                </div>
            </div>
        </div>
    </div>
</section>
{{--<section class="our-pertners">--}}
{{--    <div class="container">--}}
{{--        <h2 class="d-none">our pertners</h2>--}}
{{--        <div class="owl-carousel pertners-carousel owl-theme">--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo1.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo2.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo3.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo4.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo5.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo1.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo2.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo3.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo4.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--            <div class="item">--}}
{{--                <a href="index.html#"> <img src="frontend/img/pertners/logo5.png" alt="" /> </a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
<section class="section-wth-amwaj">
    <div class="bg_overlay_section-amwaj">
        <img src="frontend/img/bg/b_bg_02.jpg" alt="img-bg">
    </div>
    <div class="container">
        <div class="row justify-content-between mr-tp-50">
            <div class="col-md-6 side-text-right-container">
                <h2 class="side-text-right-title">We are with you ,<br> every step of the way</h2>
                <p class="side-text-right-text">
                    Whether you are looking for a <b>personal</b> website hosting plan or a <b>business</b> website hosting plan, We are the perfect solution for you. Our powerful website hosting services will not only help you achieve your overall website goals, but will also provide you with the confidence you need in knowing that you are partnered with a <a href="index.html#">reliable</a> and <a href="index.html#">secure</a> website hosting platform.
                    <br>
                    <br> We are one of the easiest website hosting platforms to use, and remain committed to providing our customers with one of the best hosting solutions on the market.
                <p>
                    <a class="side-text-right-button" href="#">start with us now</a>
            </div>
            <div class="col-md-5">
                <div class="display-on-hover-box-container">
                    <a href="#" class="display-on-hover-box-items">
                        <img src="frontend/img/svgs/hover-box/quality-badge.svg" alt="" />
                    </a>
                    <a href="#" class="display-on-hover-box-items">
                        <img src="frontend/img/svgs/hover-box/inclined-rocket.svg" alt="" />
                    </a>
                    <a href="#" class="display-on-hover-box-items">
                        <img src="frontend/img/svgs/hover-box/public-speech.svg" alt="" />
                    </a>
                    <a href="#" class="display-on-hover-box-items">
                        <img src="frontend/img/svgs/hover-box/big-light.svg" alt="" />
                    </a>
                    <a href="#" class="display-on-hover-box-items">
                        <img src="frontend/img/svgs/hover-box/big-lifesaver.svg" alt="" />
                    </a>
                    <a href="#" class="display-on-hover-box-items">
                        <img src="frontend/img/svgs/hover-box/headphones-with-thin-mic.svg" alt="" />
                    </a>
                    <a href="#" class="display-on-hover-box-items">
                        <img src="frontend/img/svgs/hover-box/inclined-paper-plane.svg" alt="" />
                    </a>
                    <a href="index.html#tab8" class="display-on-hover-box-items">
                        <img src="frontend/img/svgs/hover-box/big-telephone.svg" alt="" />
                    </a>
                    <div class="display-on-hover-box-content">
                        <div class="display-on-hover-box-cotent-items">
                            <div id="tab1" class="tab-content-hover">
                                <h5>word press hosting</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet elit. Lorem ipsum dolor sit amet</p>
                            </div>
                            <div id="tab2" class="tab-content-hover">
                                <h5>word press2 hosting</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet elit. Lorem ipsum dolor sit amet</p>
                            </div>
                            <div id="tab3" class="tab-content-hover">
                                <h5>word press3 hosting</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet elit. Lorem ipsum dolor sit amet</p>
                            </div>
                            <div id="tab4" class="tab-content-hover">
                                <h5>word press4 hosting</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet elit. Lorem ipsum dolor sit amet</p>
                            </div>
                            <div id="tab5" class="tab-content-hover">
                                <h5>word press5 hosting</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet elit. Lorem ipsum dolor sit amet</p>
                            </div>
                            <div id="tab6" class="tab-content-hover">
                                <h5>word press6 hosting</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet elit. Lorem ipsum dolor sit amet</p>
                            </div>
                            <div id="tab7" class="tab-content-hover">
                                <h5>word press7 hosting</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet elit. Lorem ipsum dolor sit amet</p>
                            </div>
                            <div id="tab8" class="tab-content-hover">
                                <h5>word press8 hosting</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet elit. Lorem ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="map-server-places-section position-relative">
    <div class="container">
        <div class="map-gene-server-place">
            <img src="frontend/img/maps/map.png" alt="" />
            <div class="server-places-spans">
                <span style="top: 34%;left: 17%;"> <b>USA Server</b> <small class="good">99.99%</small></span>
                <span style="top: 76%;left: 29%;"> <b>BRAZIL Server</b> <small class="avrage">85.70%</small></span>
                <span style="top: 50%;left: 43%;"> <b>ALGERIA Server</b> <small class="bad">40.80%</small></span>
                <span style="top: 30%;left: 70%;"> <b>ASIA Server</b> <small class="avrage">75.20%</small></span>
                <span style="top: 82%;left: 76%;"> <b>AUSTRALIA Server</b> <small class="good">95.89%</small></span>
            </div>
        </div>
        <div class="map-comment-area">
            <h3 class="map-comment-area-title">We are running in all of the world</h3>
{{--            <p class="map-comment-area-text">Our 4 million users are the 4 million reasons why you have to use our services. Each service we provide is a source of real-time intelligence about new and current threats. That’s how we run our servers to make that network even stronger.</p>--}}
        </div>
    </div>
    <div class="maps-comments-area-overflow"></div>
</section>

@include('frontend.layouts.footer')
@endsection