@extends('frontend.layouts.master')

@section('title', 'SD Host')

@section('content')
    <style>
        #SDHost-header {
            max-height: 420px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col {
            flex-grow: 1;
            padding: 0;
            width: 100%;
        }
        .newBrand .checkout-wrapper .checkout-items {
            background-color: #fff;
            padding: 20px 30px;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            float: left;
        }
        .ux-card {
            border: 1px solid #d6d6d6;
        }
         .product {
             color: #333;
             float: left;
             width: 100%;
         }
         .ux-card {
             border: 1px solid #e8e8e8;
             box-shadow: none;
             background: #fff;
             margin-bottom: 20px;
         }
        @media (min-width: 1200px){
            .ux-card {
                margin-bottom: 40px;
            }
        }

            .ux-card {
                border-radius: 4px;
                margin: 0 0 20px;
                color: #111;
                border: 1px solid #d4dbe0;
            }
        .product .product-info {
            padding: 15px 15px 10px;
            position: relative;
            border-bottom: 1px solid #e8e8e8;
        }
        .product .product-info .product-name-desc {
            float: left;
            max-width: 80%;
        }
        .product .name {
            font-weight: 700;
            padding-right: 10px;
        }
        .product .attr, .product .description, .product .duration, .product .free-for-now, .product .name, .product .renews {
            clear: left;
            float: left;
        }
         .product .attr, .product .description, .product .free-for-now, .product .renews, .product .termlength {
             color: #767676;
             font-size: 14px;
             line-height: 16px;
         }
         .product .description-wrap {
             max-width: 100%;
             overflow-wrap: break-word;
         }
         .newBrand .description {
             margin-bottom: 12px;
         }
        .description {
            margin-bottom: 40px;
        }
        .product .first-year-plus, .product .item-price-block, .product .price, .product .termlength {
            clear: right;
            float: right;
        }
        .product {
            color: #333;
            float: left;
            width: 100%;
        }
        .product .price {
            font-weight: 700;
        }
        .price {
            display: inline;
        }
        .text-primary-o {
            color: #09757a!important;
        }
        .product .first-year-plus {
            text-align: right;
            color: #767676;
            font-size: 12px;
            padding-bottom: 5px;
            padding-left: 5px;
        }
        .product .price {
            font-weight: 700;
        }
        .product .percent-off {
            font-size: .875em;
            padding-left: 8px;
        }
        .text-primary-o {
            color: #09757a!important;
        }
        .product .duration {
            width: 60%;
            padding: .5rem 0;
        }
        .stand-alone {
            width: 100%;
        }
        .stand-alone .duration-option {
            clear: left;
            float: left;
            width: 60%;
        }
        .stand-alone .duration-option .duration-select {
            width: 100%;
        }
        .dropdown {
            cursor: pointer;
        }
         .price {
             display: inline;
         }
        .text-primary-o {
            color: #09757a!important;
        }
        .product .attr, .product .description, .product .free-for-now, .product .renews, .product .termlength {
            color: #767676;
            font-size: 14px;
            line-height: 16px;
        }
        .product .remove {
            clear: right;
            float: right;
            min-width: inherit;
        }
        .newBrand .btn-link {
            color: #111!important;
        }
        .newBrand .add-privacy-crossell {
            border-top: 1px solid #d6d6d6;
            padding: 0;
        }
        .add-privacy-crossell, .display-privacy-crossell {
            /*border-top: 1px solid #e8e8e8;*/
        }
        .privacy-crosssell {
            clear: both;
            padding: 15px;
        }
            .add-privacy-container {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                align-items: stretch;
            }

        .arrow-div {
            border-radius: 0;
            margin-right: 0;
        }
        @media (min-width: 768px) {
            .arrow-div {
                display: flex;
                flex-direction: column;
                justify-content: center;
                margin-right: 15px;
                /*max-width: 215px;*/
                position: relative;
            }
        }
         .arrow-div {
             background: #ffeea9;
             border-radius: 8px;
             padding: 10px 15px;
             position: relative;
             width: 100%;
         }
        .small, small {
            font-size: .875rem;
            font-weight: 400;
        }
        b, strong {
            font-weight: bolder;
        }
        .dashed-border {
            border: none;
            border-radius: 0;
        }
        .dashed-border {
            border: 3px dashed #e8e8e8;
            text-align: center;
        }
         .dashed-border {
             border: 3px dashed #e8e8e8;
             border-radius: 8px;
             text-align: center;
         }
        .newBrand .checkout-wrapper .checkout-items .disclaimers-link {
            color: #333;
        }
        .disclaimers-link {
            color: #999;
            cursor: pointer;
            float: left;
            font-size: 12px;
            margin: 0 0 0 15px;
            text-decoration: underline;
        }
        .empty-cart-button-wrapper {
            display: block;
            float: right;
            position: relative;
            left: 15px;
        }
        .empty-cart-button-wrapper .empty-cart-button {
            margin-right: 15px;
            min-height: 1rem;
            font-weight: bold;
            color: #000;
            position: relative;
            left: 15px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .prices-purchase {
            clear: both;
            padding-top: 25px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .price-summary {
            float: right;
            width: 100%;
        }
        .subtotal {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }
        .price {
            display: inline;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .charity, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .checkout-items h2, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .promo-add-link, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .promo-container, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .subtotal, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .taxes-fees {
            padding-left: 10px;
            padding-right: 10px;
        }
         .taxes-fees {
             display: flex;
             align-items: center;
             justify-content: space-between;
         }
        .taxes-fees .btn {
            padding: unset;
            color: #000;
            font-weight: bold;
        }
         .btn-link {
             color: #111!important;
         }
        .price {
            display: inline;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .price-summary .total-banner {
            margin-top: 5px;
        }
        .total-banner {
            border-top: 1px solid #ccc;
            border-bottom: none;
            padding: 25px 20px 10px;
            font-size: 32px;
            display: flex;
            align-items: center;
            justify-content: space-between;
            font-size: 1.5rem;
            padding: 10px 10px 5px;
            white-space: nowrap;
        }
        label {
            display: inline-block;
            margin-bottom: 4px;
            font-weight: 700;
        }
        .total-banner .formatted-money-currency {
            font-size: .8em;
        }
        .price {
            display: inline;
        }
        .text-primary-o {
            color: #09757a!important;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .price-summary {
            float: right;
            width: 100%;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .prices-purchase {
            clear: both;
            padding-top: 25px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .charity, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .checkout-items h2, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .promo-add-link, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .promo-container, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .subtotal, .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .taxes-fees {
            padding-left: 10px;
            padding-right: 10px;
        }
        .checkout-wrapper .checkout-view .checkout-box .checkout-box-col .prices-purchase {
            clear: both;
            padding-top: 25px;
        }
        .checkout-items {
            background-color: #fff;
            padding: 20px 30px;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            float: left;
            width: 100%;
        }
        .btn .uxicon {
            margin-right: .35em;
            margin-left: .35em;
            vertical-align: -2px;
        }

        .uxicon {
            position: relative;
            display: inline-block;
            font-family: uxfont!important;
            font-style: normal;
            font-weight: 400;
            font-variant: normal;
            line-height: 1;
            text-decoration: none;
            text-transform: none;
            vertical-align: middle;
            speak: none;
        }
        .price-summary {
            float: right;
            width: 100%;
        }
        .sso-box {
            background-color: #fff;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            padding: 20px;
        }
        .sso-box .table-blocks {
            display: block;
        }
        .sso-box .table-blocks .sso-title {
            display: none;
            margin-left: 15px;
        }
        @media (min-width: 520px){
        .sso-box .table-blocks .sso-title {
            display: flex;
            }
        }
        .sso-box .table-blocks .sso-title .uxicon {
            display: none;
        }
         .sso-box .table-blocks .sso-title .uxicon {
             font-size: 1.5rem;
             margin: .5rem .5rem 0 0;
         }
         .sso-box h2 {
            font-size: 26px;
            font-weight: 400;
        }
        .sso-box .table-blocks .sso-flex-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap-reverse;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item {
            display: flex;
            flex: 1;
            flex-direction: column;
            padding: 0 15px 15px;
            min-width: 200px;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item .sso-question-header {
            font-weight: 700;
            font-size: 1rem;
            padding: 0;
            margin: 0;
        }
        @media (min-width: 992px){
            .h2, h2 {
                font-size: 2.02729rem;
                line-height: 40px;
            }
        }
        sso-box .table-blocks .sso-flex-container .sso-box-item .sso-text-body {
            flex-grow: 1;
            font-size: 1rem;
            padding-bottom: 10px;
        }
        .checkout-wrapper .checkout-view .c3-disclaimer button, .checkout-wrapper .checkout-view .purchase-btn button, .checkout-wrapper .checkout-view .sso-box button {
            width: 100%;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item {
            display: flex;
            flex: 1;
            flex-direction: column;
            padding: 0 15px 15px;
            min-width: 200px;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item .sso-question-header {
            font-weight: 700;
            font-size: 1rem;
            padding: 0;
            margin: 0;
        }
        .sso-box .table-blocks .sso-flex-container .sso-box-item .sso-text-body {
            flex-grow: 1;
            font-size: 1rem;
            padding-bottom: 10px;
        }
        .checkout-wrapper .checkout-view .c3-disclaimer button, .checkout-wrapper .checkout-view .purchase-btn button, .checkout-wrapper .checkout-view .sso-box button {
            width: 100%;
        }
        .btn-primary {
            color: #fff!important;
            background: #111!important;
            border-color: #111!important;
        }
        .btn-default {
            color: #111!important;
            background: #fff!important;
            border-color: #111!important;
        }
        .with-top-border {
            border-top: 1px solid #edeff2;
            background: #e4e4e4;
        }
        .empty-basket-box {
            border-radius: 8px;
            background-color: #f5f7f8;
            color: #767676;
            padding: 30px 0 40px;
            text-align: center;
        }
        @media (min-width: 401px){
            .empty-basket-box .uxicon-cart {
                color: #fd7e14;
                display: inline-block;
                font-size: 110px;
                margin: 0 15px 25px 0;
            }
        }
         .empty-basket-box .uxicon-cart {
             display: inline-block;
         }

        @media (min-width: 992px) {

            .lead {
                font-size: 1.125rem;
            }

            .lead {
                font-size: 1.125rem;
                font-weight: 300;
            }
        }
        #domain-search-card {
            background: #e4e4e4 !important;
            border-color: #d6d6d6;
            border-style: solid;
            border-width: 1px 0 0;
            margin-top: 20px;
            padding: 20px;
        }
        #domain-search-card {
            background: inherit;
            border-color: #d6d6d6;
            border-style: solid;
            border-width: 1px 0 0;
            margin-top: 20px;
            padding: 20px;
        }
         #domain-search-card {
             border-radius: 0;
         }
         #domain-search-card {
             border: 3px dashed #e8e8e8;
             margin-top: 20px;
             padding: 20px;
         }
         .ux-card {
             border: 1px solid #d6d6d6;
         }
        #domain-search-card .search-title {
            display: block;
            margin-bottom: 15px;
        }
        #domain-search-card .dpp-search h3.headline-primary {
            display: none;
        }
         #domain-search-card .dpp-search h3.headline-primary {
             display: none;
         }
        @media (min-width: 768px) {

            .dpp-search .form-group, .dpp-search h3.label {
                margin-bottom: 25px;
            }

            .dpp-search .form-group, .dpp-search h3.label {
                margin-bottom: 15px;
            }
        }
        .dpp-search button {
            font-size: 20px;
            min-width: 50px;
            padding: 0px !important;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
            position: absolute;
            bottom: 0px;
            right: 0px;
            height: 38px;
        }
        #domain-search-header .inline-button-domain-order #search-btn {
            position: relative;
        }
        .alert-success {
            width: 100%;
            color: #fff !important;
            background-color: #101010 !important;
            border-color: #ffffff !important;
            text-align: center !important;
            font-size: 18px !important;
            font-weight: bold !important;
        }
        .checkout-payment {
            margin-bottom: 20px;
        }
        .checkout-payment .payment-gray-bkg {
            background-color: #fff;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            padding: 20px;
        }
         .checkout-payment .payment-gray-bkg {
             background-color: #f5f7f8;
             border-radius: 8px;
             padding: 20px;
         }
        .payment-radios .payment-methods-header {
            display: flex;
            align-items: center;
        }
        .checkout-payment .payment-gray-bkg h4 {
            font-size: 20px;
        }
         .payment-radios .payment-methods-header h4 {
             flex-grow: 1;
         }
        .checkout-payment .stored-payments .stored-payment {
            display: inline-block;
            width: 100%;
        }
        .payment-radios .ending-with-row {
            display: inline-block;
        }
        .payment-icon {
            display: inline-block;
            vertical-align: top;
            margin-right: 5px;
        }
        .payment-icon embed, .payment-icon img {
            font-size: 24px;
            height: auto;
            vertical-align: middle;
            width: 80px;
        }
        .payment-radios .ending-with-row .payment-display {
            display: inline-block;
        }
         .checkout-payment .ending-with-row .payment-display {
             font-weight: 400;
             color: #2b2b2b!important;
             font-size: .9em;
         }
         .payment-display {
             display: inline-flex;
             -webkit-flex-direction: column;
             flex-direction: column;
             -webkit-justify-content: flex-start;
             justify-content: flex-start;
             padding-left: 10px;
         }
        checkout-payment .ending-with-row .payment-display {
            font-weight: 400;
            color: #2b2b2b!important;
            font-size: .9em;
        }
        .checkout-payment .payment-gray-bkg {
            background-color: #fff;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            padding: 20px;
        }
         .customer-contact-box {
            background-color: #fff;
            border: 1px solid #d6d6d6;
            border-radius: 4px;
            padding: 20px;
            margin-bottom: 15px;
        }
        .customer-contact-box h2 {
            font-weight: 400;
            font-size: 26px;
        }
        .customer-contact-box .customer-contact {
            background-color: inherit;
            padding: 0;
            margin-bottom: 0;
        }
         .customer-contact-box .customer-contact {
             background-color: #fff;
             border-radius: 8px;
             padding: 15px;
             margin-bottom: 15px;
         }
        .customer-contact-box .customer-contact-form .update-billing-info {
            font-weight: 700;
            margin-bottom: 10px;
        }
        .customer-contact-box .customer-contact-form .form-group {
            margin-bottom: 10px;
        }
    </style>
    <div id="SDHost-header" class="d-flex mx-auto flex-column">
        <div class="bg_overlay_header">
            <img src="frontend/img/header/h_bg_01.svg" alt="img-bg">
        </div>

        @include('frontend.layouts.topmenu')

        <div class="mt-auto header-top-height"></div>
        <main class="container mb-auto">
            <div class="row">
                <div class="col-md-8 d-flex mx-auto flex-column">
                    <div class="mb-auto"></div>
                    <h3 class="mt-3 main-header-text-title">
                        <?php echo $response; ?>
                    </h3>
                </div>
            </div>
        </main>
        <div class="mt-auto"></div>
    </div>

    <section class="padding-100-0-50">
        <div class="container">
            <h5 class="title-default-SDHost-two">
                Domain will be created once transactions is successfully approved.
                <span>Dial *182*7# and follow rules.</span>
            </h5>
        </div>
    </section>
@include('frontend.layouts.footer')
@endsection